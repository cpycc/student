package com.example.student_x_c.config;

import com.example.student_x_c.common.ApplicationContextHolder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileConfig {

    // 上传路径
    private String uploadPath;

    private String downloadPath;
    private static FileConfig instance;

    @Autowired
    public FileConfig() {
    }

    @PostConstruct
    public void init() {
        instance = this;
    }

    public static FileConfig getInstance() {
        return instance;
    }

//    private static FileConfig fileConfig = ApplicationContextHolder.getContext().getBean(FileConfig.class);

}


