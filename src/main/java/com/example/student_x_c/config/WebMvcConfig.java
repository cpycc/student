package com.example.student_x_c.config;

import com.example.student_x_c.common.JacksonObjectMapper;
import com.example.student_x_c.filter.JwtTokenAdminInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.io.File;
import java.util.List;

@Slf4j
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Autowired
    private JwtTokenAdminInterceptor jwtTokenAdminInterceptor;

    /**
     * 扩展mvc消息转换器
     * @param converters
     */
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("开始扩展mvc消息转换器...");
        //创建消息转换器
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        //设置对象转换器，底层使用jackson将Java对象转为json
        messageConverter.setObjectMapper(new JacksonObjectMapper());
        //将上面的消息转换器追加到mvc框架的转换器中
        converters.add(0,messageConverter);
    }

    /**
     * 判断放行路径
     * @param registry
     */
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtTokenAdminInterceptor)
                .addPathPatterns("/societies/**","/ad/**","/ace/**","/side/**","/main/**","/menu/**","/role/**","/shopping/**","/side/**","/societies/**","/person/**","/usershopping/**","/user/query")//不放行路径
                .excludePathPatterns(
                        "/user/login",
                        "/user/regist"
                        );//放行路径
                    }

    /**
     * 开启文件下载
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String path = this.getClass().getClassLoader().getResource("").getPath();

        File uploadDir = new File(path + "file");
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        String uploadPath = path + "file" + File.separator;

        registry.addResourceHandler("/file/**")
                .addResourceLocations("file:" + uploadPath);
    }


}
