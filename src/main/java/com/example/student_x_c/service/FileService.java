package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.ResultVo;
import com.example.student_x_c.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface FileService extends IService<User> {

    ResultVo importExcel(MultipartFile excel);

    ResultVo exportExcel(HttpServletResponse response);
}
