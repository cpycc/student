package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.common.R;
import com.example.student_x_c.dto.PermissionDto;
import com.example.student_x_c.entity.Role;
import com.example.student_x_c.entity.User;

import java.util.List;
import java.util.Set;


public interface UserService extends IService<User> {
    void removesByIds(List<Long> id);

    R<String> updatesById(Long id);

    /**
     * 获取用户权限信息
     * @param userId 用户id
     * @return 权限信息
     */
    List<PermissionDto> getUserPermission(Long userId);

    /**
     * 获取用户角色信息
     *
     * @param userId 用户id
     * @return 角色信息
     */
    List<Role> getUserRoles(Long userId);

    /**
     * 保存用户角色
     *
     * @param userId 用户id
     * @param roleIds 角色id列表
     * @return 是否成功
     */
    Boolean saveUserRoles(Long userId, Set<Long> roleIds);

}
