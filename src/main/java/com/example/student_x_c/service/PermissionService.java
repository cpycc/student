package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.Permission;

import java.util.List;
import java.util.Set;


public interface PermissionService extends IService<Permission> {


}
