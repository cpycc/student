package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.MethodLock;

public interface MethodLockService extends IService<MethodLock> {
    MethodLock findByMethodNameAndMethodDesc(String methodName, String s);
}
