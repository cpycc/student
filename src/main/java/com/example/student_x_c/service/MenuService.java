package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.Menu;


public interface MenuService extends IService<Menu> {


    /**
     * 更新菜单
     *
     * @param menu 待更新的菜单
     * @return  更新成功的菜单
     */
    Menu update (Menu menu);
}
