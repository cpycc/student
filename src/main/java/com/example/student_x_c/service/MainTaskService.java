package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.MainTask;

public interface MainTaskService extends IService<MainTask> {

}
