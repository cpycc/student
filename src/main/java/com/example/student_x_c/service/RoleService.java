package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.Permission;
import com.example.student_x_c.entity.Role;

import java.util.List;
import java.util.Set;

public interface RoleService extends IService<Role> {

    /**
     * 获取角色权限
     *
     * @param roleId 角色id
     * @return 角色权限列表
     */
    List<Permission> getPermission(Long roleId);

    /**
     * 保存角色权限
     *
     * @param roleId 角色id
     * @param menus 权限表
     * @return 是否成功
     */
    Boolean savePermission(Long roleId, Set<Long> menus);

    List<Role> findAll();
}
