package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.Permission;
import com.example.student_x_c.entity.Role;
import com.example.student_x_c.mapper.RoleMapper;
import com.example.student_x_c.service.PermissionService;
import com.example.student_x_c.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RoleMapper roleMapper;

    /**
     * 获取角色权限
     *
     * @param roleId 角色id
     * @return 角色权限列表
     */
    @Override
    public List<Permission> getPermission(Long roleId) {
        LambdaQueryWrapper<Permission> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Permission::getRoleId,roleId);
        return permissionService.list(queryWrapper);
    }


    /**
     * 保存角色权限
     *
     * @param roleId 角色id
     * @param menus 权限表
     * @return 是否成功
     */
    @Override
    @Transactional
    public Boolean savePermission(Long roleId, Set<Long> menus) {
        LambdaQueryWrapper<Permission> queryWrapper =new LambdaQueryWrapper<>();
        queryWrapper.eq(Permission::getRoleId,roleId);
        permissionService.remove(queryWrapper);

        for(Long menu:menus){
            Permission permission = new Permission(roleId, menu, LocalDateTime.now());
            permissionService.save(permission);

            if(permission == null){
                return false;
            }
        }

        return true;
    }

    @Override
    public List<Role> findAll() {
        return roleMapper.findAll();
    }
}
