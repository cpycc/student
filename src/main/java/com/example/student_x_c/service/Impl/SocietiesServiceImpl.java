package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.mapper.SocietiesMapper;
import com.example.student_x_c.service.SocietiesService;
import org.springframework.stereotype.Service;

@Service
public class SocietiesServiceImpl extends ServiceImpl<SocietiesMapper, Societies> implements SocietiesService {
}
