package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.Menu;
import com.example.student_x_c.mapper.MenuMapper;
import com.example.student_x_c.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
//    @Override
//    public Menu findByMenuPath(String path, Long pId) {
//        return MenuMapper.selectOne(new QueryWrapper<Menu>().lambda().eq(Menu::getPath, path)
//                .and(wrapper -> wrapper.eq(Menu::getPId, pId)));
//    }

    private final MenuMapper menuMapper;

    @Autowired
    private MenuService menuService;


    @Override
    public Menu update(Menu menu) {

        // 判断修改菜单的上级菜单不能是该修改菜单原有的子菜单
        if (menu.getPId() != null) {
            List<Menu> childMenus = new ArrayList<>();
            childLoop(menu.getId(), childMenus);
            if (childMenus.stream().filter(m -> m.getId().equals(menu.getPId())).count() > 0) {
                throw new RuntimeException("上级菜单不能设置为下级子菜单，防止引起嵌套循环错误！！");
            }
        }
        if (menuService.updateById(menu)) {
            return menu;
        }
        throw new RuntimeException("更新菜单失败！！");

    }


    /**
     * 返回菜单下所有的子菜单
     *
     * @param id 菜单id
     */
    private void childLoop(Long id, List<Menu> childMenus) {
        List<Menu> sysMenus = menuMapper.selectChilds(id);
        if (sysMenus == null || sysMenus.size() ==0) {
            return;
        }
        for (Menu m : sysMenus) {
            childMenus.add(m);
            childLoop(m.getId(), childMenus);
        }

    }
}
