package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.MethodLock;
import com.example.student_x_c.mapper.MethodLockMapper;
import com.example.student_x_c.service.MethodLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MethodLockServiceImpl extends ServiceImpl<MethodLockMapper, MethodLock> implements MethodLockService {

    @Autowired
    private MethodLockService methodLockService;
    /**
     *
     * @param methodName
     * @param s
     * @return
     */
    @Override
    public MethodLock findByMethodNameAndMethodDesc(String methodName, String s) {

        LambdaQueryWrapper<MethodLock> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MethodLock::getMethodName,methodName).eq(MethodLock::getMethodDesc,s);

        if(methodLockService.getOne(queryWrapper)!=null){
            return methodLockService.getOne(queryWrapper);
        }

        return null;
    }
}
