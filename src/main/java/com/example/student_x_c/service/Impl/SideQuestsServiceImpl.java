package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.SideQuests;
import com.example.student_x_c.mapper.SideQuestsMapper;
import com.example.student_x_c.service.SideQuestsService;
import org.springframework.stereotype.Service;

@Service
public class SideQuestsServiceImpl extends ServiceImpl<SideQuestsMapper, SideQuests> implements SideQuestsService {
}
