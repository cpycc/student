package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.AdCategory;
import com.example.student_x_c.mapper.AdCategoryMapper;
import com.example.student_x_c.service.AdCategoryService;
import org.springframework.stereotype.Service;

@Service
public class AdCategoryServiceImpl extends ServiceImpl<AdCategoryMapper, AdCategory> implements AdCategoryService {
}
