package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.UserShopping;
import com.example.student_x_c.mapper.UserShoppingMapper;
import com.example.student_x_c.service.UserShoppingService;
import org.springframework.stereotype.Service;

@Service
public class UserShoppingServiceImpl extends ServiceImpl<UserShoppingMapper, UserShopping> implements UserShoppingService {
}
