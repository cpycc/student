package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.Employee;
import com.example.student_x_c.mapper.EmployeeMapper;
import com.example.student_x_c.service.EmployeeService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}
