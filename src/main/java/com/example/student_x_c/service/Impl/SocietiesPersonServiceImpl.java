package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.SocietiesPerson;
import com.example.student_x_c.mapper.SocietiesPersonMapper;
import com.example.student_x_c.service.SocietiesPersonService;
import org.springframework.stereotype.Service;

@Service
public class SocietiesPersonServiceImpl extends ServiceImpl<SocietiesPersonMapper, SocietiesPerson> implements SocietiesPersonService {
}
