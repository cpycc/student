package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.entity.MainTask;
import com.example.student_x_c.mapper.MainTaskMapper;
import com.example.student_x_c.service.MainTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainTaskServiceImpl extends ServiceImpl<MainTaskMapper, MainTask> implements MainTaskService {

}
