package com.example.student_x_c.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.common.R;
import com.example.student_x_c.dto.PermissionDto;
import com.example.student_x_c.entity.*;
import com.example.student_x_c.mapper.UserMapper;
import com.example.student_x_c.mapper.UserRoleMapper;
import com.example.student_x_c.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
//@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserMapper userMapper;

    @Autowired
    private UserShoppingService userShoppingService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private SocietiesPersonService societiesPersonService;

    @Autowired
    private MainSideService mainSideService;

    /**
     * 删除用户以及相关数据
     * @param id
     */
    @Override
    @Transactional
    public void removesByIds(List<Long> id) {

        userService.removeByIds(id);

        LambdaQueryWrapper<UserShopping> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(UserShopping::getUserId,id);
        userShoppingService.remove(queryWrapper);

        LambdaQueryWrapper<MainSide> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.in(MainSide::getUserId,id);
        mainSideService.remove(queryWrapper1);

        LambdaQueryWrapper<SocietiesPerson> queryWrapper2 = new LambdaQueryWrapper<>();
        queryWrapper2.in(SocietiesPerson::getUserId,id);
        societiesPersonService.remove(queryWrapper2);


    }

    /**
     * 更新用户信息
     * @param id
     */
    @Override
    public R<String> updatesById(Long id) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId,id);
        User user = userService.getOne(queryWrapper);

        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        if(user.getStatus() == 1){
            updateWrapper.eq(User::getId,id);
            updateWrapper.set(User::getStatus,0);
            userService.update(updateWrapper);
        }else if(user.getStatus() == 0){
            updateWrapper.eq(User::getId,id);
            updateWrapper.set(User::getStatus,1);
            userService.update(updateWrapper);
        }
        return R.success("更新成功");
    }

    @Override
    public List<PermissionDto> getUserPermission(Long userId) {
        return userMapper.selectUserPermission(userId);
    }

    @Override
    public List<Role> getUserRoles(Long userId) {
        return userMapper.selectUserRoles(userId);
    }

    @Override
    public Boolean saveUserRoles(Long userId, Set<Long> roleIds) {
        // 首先清除该用户原有的角色信息
        LambdaQueryWrapper<UserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRole::getUserId, userId);
        userRoleService.remove(queryWrapper);
        // 再进行添加
        for (Long roleId : roleIds) {
            UserRole UserRole = new UserRole();
            UserRole.setUserId(userId);
            UserRole.setRoleId(roleId);
            UserRole.setCreateTime(LocalDateTime.now());
            userRoleService.save(UserRole);
        }

        return true;
    }
}
