package com.example.student_x_c.service.Impl;



import com.example.student_x_c.entity.ExcelWriteVo;
import com.example.student_x_c.entity.User;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.utils.ExcelUtils;
import org.apache.commons.lang.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student_x_c.common.ParamErrorException;
import com.example.student_x_c.entity.ResultVo;
import com.example.student_x_c.mapper.FileMapper;
import com.example.student_x_c.service.FileService;
import com.example.student_x_c.utils.FileUtils;
import com.example.student_x_c.utils.ResultVoUtils;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.example.student_x_c.utils.FileUtils.downloadFile;


@Slf4j
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, User> implements FileService {

    @Autowired
    private UserService userService;

    @Autowired
    private ExcelUtils excelUtils;
    @Override
    public ResultVo importExcel(MultipartFile file) {
        // 1.可做入参校验，这里省略
        // 2.上传至服务器某路径下
        ResultVo resultVo = uploadFile(file);
        if (!resultVo.checkSuccess()) {
            return resultVo;
        }
        String filePath = (String)resultVo.getData();
        if (StringUtils.isBlank(filePath)) {
            return ResultVoUtils.error("【导入Excel文件】生成的Excel文件的路径为空");
        }
        // 3.读取excel文件
        List<User> excelVos = excelUtils.simpleExcelRead(filePath, User.class);
        if (CollectionUtils.isEmpty(excelVos) || excelVos.size() < 2) {
            log.error("【导入Excel文件】上传Excel文件{}为空", file.getOriginalFilename());
            return ResultVoUtils.error("上传Excel文件为空");
        }
        // 4.删除临时文件
        boolean deleteFile = FileUtils.deleteFile(new File(filePath));
        if (!deleteFile) {
            log.error("【导入Excel文件】删除临时文件失败，临时文件路径为{}", filePath);
            return ResultVoUtils.error("删除临时文件失败");
        }
        log.info("【导入Excel文件】删除临时文件成功，临时文件路径为：{}", filePath);
        return ResultVoUtils.success(excelVos);
    }



    // 上传文件
    public ResultVo<String> uploadFile(MultipartFile file) {
        log.info("【文件上传】进入到文件上传方法");
        // 1.参数校验
        if (null == file || file.isEmpty()) {
            log.error("【文件上传】文件为空!");
            throw new ParamErrorException();
        }
        // 2.上传文件
        ResultVo<String> resultVo = FileUtils.uploadFile(file);
        return resultVo;
    }

    @Override
    public ResultVo exportExcel(HttpServletResponse response) {
        // 1.根据查询条件获取结果集
        List<ExcelWriteVo> excelWriteVos = getExcelWriteVoListByCondition();
        if (CollectionUtils.isEmpty(excelWriteVos)) {
            log.info("【导出Excel文件】要导出的数据为空，无法导出！");
            return ResultVoUtils.success("数据为空");
        }
        // 2.获取要下载Excel文件的路径
        ResultVo<String> resultVo = getDownLoadPath(ExcelWriteVo.class, excelWriteVos);
        if (!resultVo.checkSuccess()) {
            log.error("【导出Excel文件】获取要下载Excel文件的路径失败");
            return resultVo;
        }
        // 3.下载Excel文件
        String fileDownLoadPath = resultVo.getData();
        ResultVo<String> downLoadResultVo = downloadFile(new File(fileDownLoadPath), response);
        if (null != downLoadResultVo && !downLoadResultVo.checkSuccess()) {
            log.error("【导出Excel文件】下载文件失败");
            return downLoadResultVo;
        }
        // 4.删除临时文件
        boolean deleteFile = FileUtils.deleteFile(new File(fileDownLoadPath));
        if (!deleteFile) {
            log.error("【导入Excel文件】删除临时文件失败，临时文件路径为{}", fileDownLoadPath);
            return ResultVoUtils.error("删除临时文件失败");
        }
        log.info("【导入Excel文件】删除临时文件成功，临时文件路径为：{}", fileDownLoadPath);
        return null;
    }


    /**
     * 查询表
     * @return
     */
    public List<ExcelWriteVo> getExcelWriteVoListByCondition() {
        List<ExcelWriteVo> excelWriteVos = new ArrayList<>();
        List<User> users = userService.list();
        for (User user : users) {
            excelWriteVos.add(new ExcelWriteVo( user.getName(), user.getPhone(),user.getStudentId(),  user.getSex(), user.getIdNumber(),user.getAddress(),user.getAge(),user.getAcademyName(), user.getTask()));
        }
       return excelWriteVos;
    }

    public ResultVo<String> getDownLoadPath(Class<ExcelWriteVo> clazz, List<ExcelWriteVo> excelWriteVos) {
        String downLoadPath = FileUtils.getDownLoadPath();
        if (StringUtils.isBlank(downLoadPath)) {
            log.error("【导出Excel文件】生成临时文件失败");
            return ResultVoUtils.error("生成临时文件失败");
        }
        // 1.创建一个临时目录
        FileUtils.mkdirs(downLoadPath);
        String fullFilePath = downLoadPath + File.separator + System.currentTimeMillis() + "." + ExcelUtils.EXCEL_2007;
        log.info("【导出Excel文件】文件的临时路径为：{}", fullFilePath);
        // 2.写入数据
        excelUtils.simpleExcelWrite(fullFilePath, clazz, excelWriteVos);
        return ResultVoUtils.success(fullFilePath);
    }



}

