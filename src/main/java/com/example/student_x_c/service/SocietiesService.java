package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.Societies;

public interface SocietiesService extends IService<Societies> {
}
