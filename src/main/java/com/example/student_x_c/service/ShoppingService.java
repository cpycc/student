package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.Shopping;

import java.util.List;

public interface ShoppingService extends IService<Shopping> {

}
