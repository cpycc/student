package com.example.student_x_c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student_x_c.entity.UserShopping;

public interface UserShoppingService extends IService<UserShopping> {
}
