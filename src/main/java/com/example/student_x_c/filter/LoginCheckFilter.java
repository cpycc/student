package com.example.student_x_c.filter;

import com.alibaba.fastjson.JSON;
import com.example.student_x_c.common.BaseContext;
import com.example.student_x_c.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 检查用户是否已经完成登录
 */
@WebFilter(filterName = "loginCheckFilter",urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    //路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        //1.获取本次请求uri
        String requestURI = request.getRequestURI();

        //直接放行的请求
        String[] urls = new String[]{
                "/user/login",
                "/user/regist",
                "/common/**"
        };

        //2.判断本次请求是否需要处理
        boolean check = check(urls, requestURI);

        //3.如果不需要处理，直接放行
        if (check) {
            filterChain.doFilter(request, response);
            return;
        }

        //4-1.判断登陆状态，如果已经登陆，则放行
//        if (request.getSession().getAttribute("employee") != null) {
//
//            Long empId = (Long) request.getSession().getAttribute("employee");
//            BaseContext.setCurrentId(empId);
//
//            filterChain.doFilter(request, response);
//            return;
//        }

        //4-2.判断登陆状态，如果已经登陆，则放行
        log.info("sessionId:{}",request.getSession().getId());

        if (request.getSession().getAttribute("user") != null) {

            Long userId = (Long) request.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);

            filterChain.doFilter(request, response);
            return;
        }

        //5.如果没有登录，返回未登录结果,通过输出流方式对客户端响应数据
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));


        log.info("拦截到请求：{}", request.getRequestURI());
        return;
    }

    /**
     * 路劲匹配检查请求是否放行
     *
     * @param urls
     * @param requestURI
     * @return
     */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            if (PATH_MATCHER.match(url, requestURI)) {
                return true;
            }
        }
        return false;
    }
}

