package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 数据库排斥锁信息
 */

@Data
public class MethodLock {
    private static final long serialVersionUID = 1L;

    private Long id;
    //被锁方法名
    private String methodName;
    //占用线程描述
    private String methodDesc;
    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}
