package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 主副任务联表
 */
@Data
public class MainSide implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //主任务id
    private Long mainId;
    //学生id
    private Long userId;
    //名称
    private String name;
    //分数
    private Integer fraction;
    //任务图片
    private String image;
    //任务提交状态
    private Integer sub;
    //学生名字
    private String userName;
    //不通过理由
    private String content;


    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
