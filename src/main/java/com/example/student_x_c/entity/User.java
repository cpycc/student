package com.example.student_x_c.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 学生表
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //账号
    private String name;
    //密码
    private String password;
    //电话
    private String phone;
    //学号
    private String studentId;
    //性别
    private Integer sex;
    //身份证
    private String idNumber;
    //地址
    private String address;
    //年龄
    private String age;
    //状态
    private Integer status;
    //头像
    private String avatar;
    //录取通知书
    private String letterAcceptance;
    //权限
    private String role;
    //学院
    private String academyName;
    //任务积分
    private Integer task;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

//    public boolean checkSuccess() {
//    }
}
