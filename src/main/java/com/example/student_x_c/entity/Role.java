package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色表
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //名称
    private String name;
    //编码
    private String code;
    //描述
    private String description;

    //状态
    private Integer enable;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Builder.Default
    private LocalDateTime updateTime = LocalDateTime.now();


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
