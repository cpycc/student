package com.example.student_x_c.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.type.JdbcType;

import java.time.LocalDateTime;

/**
 * 菜单表
 */
@Data
@NoArgsConstructor
@SuperBuilder
public class Menu {
    //主键
    private Long id;
    //菜单名称：用户管理
    private String title;
    //路由路径
    private String path;
    //组件路径
    private String component;
    //图标
    private String icon;
    //排序
    private Integer sort;
    //路由名称：User
    private String name;
    //是否启用
    @Builder.Default
    private Boolean enable = true;
//    是否缓存
    private Boolean noCache;
//    跳转
    private String redirect;
    //是否隐藏  权限分配的时候需要隐藏
    private Boolean hidden;
    //菜单等级-暂时没用上，先留着
    private Integer level;



    //父id
    @TableField(value = "p_id", updateStrategy = FieldStrategy.IGNORED,jdbcType = JdbcType.BIGINT)
    private Long pId;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Builder.Default
    private LocalDateTime updateTime= LocalDateTime.now();


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser ;


    public String getLabel() {
        return title;
    }

}
