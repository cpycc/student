package com.example.student_x_c.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.example.student_x_c.utils.SexConvert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelWriteVo {
    private static final long serialVersionUID = 1L;

    //账号
    private String name;
    //电话
    private String phone;
    //学号
    private String studentId;
    //性别
    @ExcelProperty(converter = SexConvert.class)
    private Integer sex;
    //身份证
    private String idNumber;
    //地址
    private String address;
    //年龄
    private String age;
    //学院
    private String academyName;
    //任务积分
    private Integer task;

//    // 创建时间
//    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
//    private String createTime;
}
