package com.example.student_x_c.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Permission {

    private static final long serialVersionUID = 1L;

    private Long id;
    //角色id
    private Long roleId;
    //菜单id
    private Long menuId;



    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    public Permission(Long roleId, Long menuId, LocalDateTime createTime) {
        this.roleId = roleId;
        this.menuId = menuId;
        this.createTime = createTime;
    }
}
