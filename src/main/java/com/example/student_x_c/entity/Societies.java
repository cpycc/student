package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 社团表
 */
@Data
public class Societies implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //社团名
    private String name;
    //社长姓名
    private String presidentName;
    //社团简介
    private String interpretation;
    //地址
    private String address;
    //社团人数
    private String number;
    //社团图片
    private String image;
    //联系方式
    private String contact;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
