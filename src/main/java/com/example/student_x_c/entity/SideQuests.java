package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 支线任务表
 */
@Data
public class SideQuests implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //名称
    private String name;
    //图片
    private String image;
    //开始时间
    private LocalDateTime beginTime;
    //结束时间
    private LocalDateTime endTime;
    //状态
    private Integer status;
    //分数
    private Integer fraction;
    //分类id
    private Integer categoryId;
    //排序
    private Integer sort;
    //描述
    private String description;
    //学生id
    private Long userId;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
