package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 学生兑换商品表
 */
@Data
public class UserShopping implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //用户id
    private Long userId;
    //商品id
    private Long shoppingId;
    //商品名
    private String name;
    //商品价格
    private Integer price;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

}
