package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 广告表
 */
@Data
public class Ad implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //广告名称
    private String name;
    //广告商家
    private String business;
    //广告图片
    private String adImage;
    //广告内容
    private String adMessage;
    //来源地分类
    private String addressType;
    //性别分类
    private String sexType;
    //学院分类
    private String studentType;

    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

}
