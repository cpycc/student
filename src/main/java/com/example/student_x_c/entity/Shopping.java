package com.example.student_x_c.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商品表
 */
@Data
public class Shopping implements Serializable {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //名称
    private String name;
    //所需积分
    private Integer price;
    //数量
    private Integer number;
    //图片
    private String image;
    //描述
    private String contect;
    //商品备选情况
    private Integer status;


    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
