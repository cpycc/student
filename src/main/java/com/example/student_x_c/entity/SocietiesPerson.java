package com.example.student_x_c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 社团人员
 */
@Data
public class SocietiesPerson {
    private static final long serialVersionUID = 1L;
    //id
    private Long id;
    //姓名
    private String name;
    //电话
    private String phone;
    //性别
    private Integer sex;
    //学号
    private String studentId;
    //社团id
    private Long societiesId;
    //审核状态
    private Integer status;
    //学生id
    private Long userId;


    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    //创建人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    //修改人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

}
