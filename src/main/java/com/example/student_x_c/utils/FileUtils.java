package com.example.student_x_c.utils;

import com.example.student_x_c.config.FileConfig;
import org.apache.commons.lang.StringUtils;
import com.example.student_x_c.entity.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

@Slf4j
public class FileUtils {
    @Autowired
    private static FileConfig fileConfig;

    // 下划线
    public static final String UNDER_LINE = "_";

    // 上传文件
    public static ResultVo<String> uploadFile(MultipartFile file) {
        // 1.获取一个新的文件名
        String newFileName = getNewFileName(file);
        if (StringUtils.isBlank(newFileName)) {
            log.error("【上传文件】转换文件名称失败");
            return ResultVoUtils.error("【上传文件】转换文件名称失败");
        }
        // 2.获取文件上传路径
        String uploadPath = "E:\\temp";
        if (StringUtils.isBlank(uploadPath)) {
            log.error("【上传文件】获取文件上传路径失败");
            return ResultVoUtils.error("【上传文件】获取文件上传路径失败");
        }
        uploadPath = uploadPath + File.separator + DateUtils.getCurrentDate();
        // 3.生成上传目录
        File uploadDir = mkdirs(uploadPath);
        if (!uploadDir.exists()) {
            log.error("【上传文件】生成上传目录失败");
            return ResultVoUtils.error("【上传文件】生成上传目录失败");
        }
        // 4.文件全路径
        String fileFullPath = uploadPath + File.separator + newFileName;
        log.info("上传的文件：" + file.getName() + "，" + file.getContentType() + "，保存的路径为：" + fileFullPath);
        try {
            // 5.上传文件
            doUploadFile(file, fileFullPath);
        } catch (IOException e) {
            log.error("【上传文件】上传文件报IO异常，异常信息为{}", e.getMessage());
            return ResultVoUtils.error(e.getMessage());
        }
        return ResultVoUtils.success(fileFullPath);
    }

    /**
     * @param file:
     * @param path:
     * @Description:上传文件
     * @Author: zzc
     * @Date: 2021-11-11 0:05
     * @return: void
     **/
    public static void doUploadFile(MultipartFile file, String path) throws IOException {

        Streams.copy(file.getInputStream(), new FileOutputStream(path), true);

    }

    /**
     * @param path:
     * @Description:递归生成父级路径
     * @Author: zzc
     * @Date: 2021-11-10 23:55
     * @return: void
     **/
    public static File mkdirs(String path) {
        File file = new File(path);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
        return file;
    }

    /**
     * @param file:
     * @Description:将上传的文件转换为一个新的文件名
     * @Author: zzc
     * @Date: 2021-11-10 23:17
     * @return: java.lang.String
     **/
    public static String getNewFileName(MultipartFile file) {
        // 1.获取上传的文件名称（包含后缀。如：test.jpg）
        String originalFilename = file.getOriginalFilename();
        log.info("【上传文件】上传的文件名为{}", originalFilename);
        // 2.以小数点进行分割
        String[] split = originalFilename.split("\\.");
        String newFileName = null;
        if (null == split || split.length == 0) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        if (1 == split.length) {
            // 3.此文件无后缀
            newFileName = builder.append(originalFilename).append(UNDER_LINE).append(System.nanoTime()).toString();
            return newFileName;
        }
        // 4.获取文件的后缀
        String fileSuffix = split[split.length - 1];
        for (int i = 0; i < split.length - 1; i++) {
            builder.append(split[i]);
            if (null != split[i + 1] && "" != split[i + 1]) {
                builder.append(UNDER_LINE);
            }
        }
        newFileName = builder.append(System.nanoTime()).append(".").append(fileSuffix).toString();
        return newFileName;
    }

    /**
     * @param file:
     * @param response:
     * @Description:下载文件
     * @Author: zzc
     * @Date: 2021-11-12 0:01
     * @return: com.zzc.hardcore.vo.ResultVo<java.lang.String>
     **/
    public static ResultVo<String> downloadFile(File file, HttpServletResponse response) {
        try {
            // 1.设置响应头
            setResponse(file, response);
        } catch (UnsupportedEncodingException e) {
            log.error("文件名{}不支持转换为字符集{}", file.getName(), "UTF-8");
            return ResultVoUtils.error(e.getMessage());
        }
        // 2.下载文件
        return doDownLoadFile(file, response);
    }

    /**
     * @param file:
     * @param response:
     * @Description:下载文件
     * @Author: zzc
     * @Date: 2021-11-11 22:51
     * @return:
     **/
    public static ResultVo<String> doDownLoadFile(File file, HttpServletResponse response) {

        try (InputStream in = new FileInputStream(file);
             OutputStream out = response.getOutputStream()) {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            log.info("【文件下载】文件下载成功");
            return null;
        } catch (FileNotFoundException e) {
            log.error("【文件下载】下载文件时，没有找到相应的文件，文件路径为{}", file.getAbsolutePath());
            return ResultVoUtils.error(e.getMessage());
        } catch (IOException e) {
            log.error("【文件下载】下载文件时，出现文件IO异常");
            return ResultVoUtils.error(e.getMessage());
        }
    }

    /**
     * @param file:
     * @param response:
     * @Description:设置响应头
     * @Author: zzc
     * @Date: 2021-11-11 22:44
     * @return: void
     **/
    public static void setResponse(File file, HttpServletResponse response) throws UnsupportedEncodingException {
        // 清空response
        response.reset();
        response.setCharacterEncoding("UTF-8");
        // 返回给客户端类型，任意类型
        response.setContentType("application/octet-stream");
        // Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
        // attachment表示以附件方式下载 inline表示在线打开 "Content-Disposition: inline; filename=文件名.mp3"
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), "UTF-8"));
        // 告知浏览器文件的大小
        response.addHeader("Content-Length", String.valueOf(file.length()));
    }


    /**
     * @param file:
     * @Description:递归删除目录下的所有文件及子目录下所有文件
     * @Author: zzc
     * @Date: 2021-11-14 18:34
     * @return: boolean
     **/
    public static boolean deleteFile(File file) {
        if (!file.exists()) {
            return false;
        }
        if (file.isDirectory()) {
            String[] children = file.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteFile(new File(file, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return file.delete();
    }

    // 获取文件下载时生成文件的路径
    public static String getDownLoadPath() {
        return fileConfig.getDownloadPath();
    }






}


