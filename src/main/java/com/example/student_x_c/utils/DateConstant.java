package com.example.student_x_c.utils;

public interface DateConstant {

    // 默认的日期格式化格式
    String DEFAULT_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";

}
