package com.example.student_x_c.utils;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.common.auth.CredentialsProviderFactory;
import com.aliyun.oss.common.auth.EnvironmentVariableCredentialsProvider;
import com.aliyun.oss.model.*;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * 阿里云OSS文件上传工具类
 */
@Component
public class AliOSSUtils {

    @Value("${aliOSS-utils.endpoint}")
    private String endpoint;    //服务端点
    @Value("${aliOSS-utils.bucketName}")
    private String bucketName;  //bucket名称
//    @Value("${aliOSS-utils.packageName01}")
//    private String packageName01;   //对象存储二级包路径

    /**
     * 文件上传
     * @param file
     * @return
     * @throws Exception
     */
    public String upload(MultipartFile file) throws Exception {

        // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
        EnvironmentVariableCredentialsProvider credentialsProvider = CredentialsProviderFactory.newEnvironmentVariableCredentialsProvider();

        //获取上传文件的输入流
        InputStream inputStream = file.getInputStream();

        //使用UUID通用唯一识别码 + 后缀名的形式 防止文件名重复导致覆盖
        String originalFileName = file.getOriginalFilename();
        //增加断言 null值抛出异常
        assert originalFileName != null;

        String fileName = UUID.randomUUID().toString() + originalFileName.substring(originalFileName.lastIndexOf("."));

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, credentialsProvider);

        //创建对象名称（对象存储的位置）
        String objectName = fileName;

        //存储文件访问路径
        String url = null;

        //上传文件到OSS
        try {
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream);

            // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
             ObjectMetadata metadata = new ObjectMetadata();
            // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
             metadata.setObjectAcl(CannedAccessControlList.PublicRead);
             putObjectRequest.setMetadata(metadata);

            // 上传文件。
            PutObjectResult result = ossClient.putObject(putObjectRequest);

            //拼接文件访问路径并返回  在endpoint名称中加入bucket名称 最后拼接上文件名
            url = fileName;

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {

            inputStream.close();

            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return url;
    }


    /**
     * 获取图片
     * @param fileName
     * @return
     */
    public String getOrReloadImage(String fileName) {

        String imageUrl = null;

        String path = this.getClass().getClassLoader().getResource("").getPath();

        File uploadDir = new File(path + "file");
        if(!uploadDir.exists()){
            uploadDir.mkdir();
        }

        String uploadPath = path + "file" + File.separator + fileName;

        File img = new File(uploadPath);
        if (!img.exists()) {
            String accessKeyId = "LTAI5tFRnt1ziH9xNsEtqgQJ";
            String accessKeySecret = "B6TWAvspUfcrHeXqvb6JZSAQjpb9CO";

            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            try {
                OSSObject ossObject = ossClient.getObject(bucketName, fileName);
                InputStream inputStream = ossObject.getObjectContent();
                try (OutputStream outStream = new FileOutputStream(uploadPath)) {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            } finally {
                ossClient.shutdown();
            }
        }


        imageUrl = "/file/" + fileName;
        return imageUrl;
    }

    /**
     * 对图像路径处理
     * @param image
     */
    public void refreshFile(String image) {
        if (ObjectUtils.isEmpty(image)) {
            return;
        }
        int separatorIndex = image.lastIndexOf("/");
        String fileName = image.substring(separatorIndex + 1);
        getOrReloadImage(fileName);
    }

    /**
     * 获得微信小程序路径
     * @param fileName
     * @return
     */
    public String getFileUrl(String fileName) {
        int separatorIndex = fileName.lastIndexOf("/");
        fileName = fileName.substring(separatorIndex + 1);

        return fileName;

    }
}