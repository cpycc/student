package com.example.student_x_c.utils;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;



@Component
public class DateUtils {
    // 获取当前时间
    public static String getCurrentTime() {
        DateTime now = new DateTime();
        return now.toString(DateConstant.DEFAULT_FORMAT_PATTERN);
    }

    // 获取当前日期
    public static String getCurrentDate() {
        LocalDate localDate = new LocalDate();
        return localDate.toString();
    }


}

