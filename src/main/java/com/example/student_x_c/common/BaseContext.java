package com.example.student_x_c.common;

/**
 * 基于ThreadLocal封装工具，用户保存和获取当前登录id
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置当前登录id
     * @param id
     */
    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    /**
     * 获取当前登录id
     * @return
     */
    public static Long getCurrentId(){
        return threadLocal.get();
    }

    /**
     * 删除当前登录id
     */
    public static void removeCurrentId(){
        threadLocal.remove();
    }
}
