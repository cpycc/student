package com.example.student_x_c.common;

import com.example.student_x_c.entity.ResultCodeEnum;
import lombok.Data;

@Data
public class ParamErrorException extends RuntimeException{

    // 错误码
    private Integer code;

    // 错误消息
    private String msg;

    public ParamErrorException() {
        this(ResultCodeEnum.PARAM_ERROR.getCode(), ResultCodeEnum.PARAM_ERROR.getMessage());
    }
    public ParamErrorException(String msg) {
        this(ResultCodeEnum.PARAM_ERROR.getCode(), msg);
    }
    public ParamErrorException(Integer code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
