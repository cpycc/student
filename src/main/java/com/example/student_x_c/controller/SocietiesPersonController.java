package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.MainSide;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.entity.SocietiesPerson;
import com.example.student_x_c.service.SocietiesPersonService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 社团成员
 */
@RestController
@RequestMapping("/person")
public class SocietiesPersonController {

    @Autowired
    private SocietiesPersonService societiesPersonService;

    /**
     * 添加社团成员
     * @param societiesPerson
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<String> add(@RequestBody SocietiesPerson societiesPerson){
        societiesPersonService.save(societiesPerson);
        return R.success("添加成功");
    }


    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "societiesPerson", key = "#name+'_'+'societiesPerson'")
    public R<Page> page(int page, int pageSize, String name) {
        Page<SocietiesPerson> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<SocietiesPerson> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),SocietiesPerson::getName,name);
        queryWrapper.orderByDesc(SocietiesPerson::getUpdateTime);
        societiesPersonService.page(pageInfo,queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 通过id查询学生参加的社团
     * @param page
     * @param pageSize
     * @param id
     * @return
     */
    @PostMapping("/pageR")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<Page> pageR(int page, int pageSize, Long id) {
        Page<SocietiesPerson> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<SocietiesPerson> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SocietiesPerson::getUserId,id);
        queryWrapper.eq(SocietiesPerson::getStatus,2);
        queryWrapper.orderByDesc(SocietiesPerson::getUpdateTime);
        societiesPersonService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }



    /**
     * 批量删除成员
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<String> delete(List<Long> ids){
        societiesPersonService.removeByIds(ids);
        return R.success("删除成功");
    }


    /**
     * 回传社团成员信息（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<SocietiesPerson> update(@PathVariable Long id){
        SocietiesPerson societiesPerson = societiesPersonService.getById(id);
        return R.success(societiesPerson);
    }
    /**
     * 修改社团成员数据
     * @param societiesPerson
     * @return
     */
    @PostMapping("/update")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<String> update(@RequestBody SocietiesPerson societiesPerson){
        societiesPersonService.updateById(societiesPerson);
        return R.success("修改成功");
    }

    /**
     * 通过两个Id查询任务信息
     * @param societiesId
     * @return
     */
    @PostMapping("/select")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<Integer> selectById(Long societiesId,Long userId) {
        LambdaQueryWrapper<SocietiesPerson> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SocietiesPerson::getSocietiesId,societiesId);
        queryWrapper.eq(SocietiesPerson::getUserId,userId);
        SocietiesPerson societiesPerson = societiesPersonService.getOne(queryWrapper);
        if(societiesPerson == null){
            return R.error("没有相关数据");
        }

        Integer status = societiesPerson.getStatus();
        return R.success(status);
    }

    /**
     * 审核通过
     * @param ids
     * @return
     */
    @PostMapping("/updateStatusYes")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<String> updateStatusYes(@RequestParam("ids") List<Long> ids){

        LambdaUpdateWrapper<SocietiesPerson> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(SocietiesPerson::getId,ids);
        updateWrapper.set(SocietiesPerson::getStatus,2);
        societiesPersonService.update(updateWrapper);
        return R.success("审核通过");

    }

    /**
     * 审核不通过
     * @param ids
     * @return
     */
    @PostMapping("/updateStatusNo")
    @CacheEvict(value = "societiesPerson",allEntries = true)
    public R<String> updateStatusNo(@RequestParam("ids") List<Long> ids){

        LambdaUpdateWrapper<SocietiesPerson> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(SocietiesPerson::getId,ids);
        updateWrapper.set(SocietiesPerson::getStatus,3);
        societiesPersonService.update(updateWrapper);
        return R.success("审核不通过");

    }

}
