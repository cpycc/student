package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.example.student_x_c.common.R;


import com.example.student_x_c.entity.ResultVo;
import com.example.student_x_c.entity.SideQuests;
import com.example.student_x_c.entity.User;
import com.example.student_x_c.service.FileService;
import com.example.student_x_c.service.MainTaskService;
import com.example.student_x_c.service.SideQuestsService;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * 处理图片上传下载，excel文件上传下载
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {

    @Value("${student.path}")
    private String basePath;

    @Autowired
    private FileService fileService;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private MainTaskService mainTaskService;

    @Autowired
    private SideQuestsService sideQuestsService;


    /**
     *excel文件导入
     * @param excel
     * @return
     */
    @PostMapping("/importExcel")
    public ResultVo importExcel(@RequestParam("file") MultipartFile excel) {
        return fileService.importExcel(excel);
    }

    /**
     * excel文件导出
     * @param response
     * @return
     */
    @PostMapping("/exportExcel")
    public ResultVo exportExcel(final HttpServletResponse response) {
        return fileService.exportExcel(response);
    }


    /**
     * 图片上传
     * @return
     * @throws Exception
     */
    @PostMapping("/upload")
    public R<String> upload(@RequestParam("file") MultipartFile avatar) throws Exception {

        //调用工具类方法将文件存入阿里云OSS
        String fileName = aliOSSUtils.upload(avatar);

        // 设置响应头
        String url = aliOSSUtils.getOrReloadImage(fileName);


        return R.success(url);
    }




    /**
     * 文件上传
     * @param file
     * @return
     */
//    @PostMapping("/upload")
//    public R<String> upload(MultipartFile file){
//        //file是一个临时文件，需要转存到指定位置，否则请求结束自动删除
//        log.info(file.toString());
//
//        //原始文件名
//        String originalFilename = file.getOriginalFilename();
//        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
//
//        //使用UUID重新生成文件名，防止文件名重复
//        String fileName = UUID.randomUUID().toString()+suffix;
//
//
//        //创建一个目录对象
//        File dir = new File(basePath);
//        //判断当前目录是否存在,不存在则创建一个目录
//        if(!dir.exists()){
//            dir.mkdirs();
//        }
//
//        try {
//            file.transferTo(new File(basePath+fileName));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return R.success(fileName);
//    }

    /**
     * 文件下载
     * @param name
     *  @param response
     */
//    @GetMapping("/download")
//    public void download(String name, HttpServletResponse response){
//
//        try {
//            //输入流读取文件内容
//            FileInputStream fileInputStream = new FileInputStream(new File(basePath+name));
//
//            //输出流，将文件写回浏览器，在浏览器显示图片
//            ServletOutputStream outputStream = response.getOutputStream();
//
//            //设置响应文件类型
//            response.setContentType("image/jpeg");
//
//            int len = 0;
//            byte[] bytes = new byte[1024];
//            while((len = fileInputStream.read(bytes)) != -1){
//                outputStream.write(bytes,0,len);
//                outputStream.flush();
//            }
//
//            //关闭资源
//            outputStream.close();
//            fileInputStream.close();
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//
//
//    }


    /**
     * 任务总数
     * @return
     */
    @PostMapping("/all")
    public R<Integer> all() {
        int count1 = mainTaskService.count();
        int count2 = sideQuestsService.count();
        int count = count1 + count2;
        return R.success(count);
    }


}
