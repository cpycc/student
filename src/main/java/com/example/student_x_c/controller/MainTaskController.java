package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.MainTask;
import com.example.student_x_c.entity.SideQuests;
import com.example.student_x_c.service.MainTaskService;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/main")
public class MainTaskController {

    @Autowired
    private MainTaskService mainTaskService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AliOSSUtils aliOSSUtils;



    /**
     * 添加主任务
     * @param mainTask
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<String> add(@RequestBody MainTask mainTask){
        log.info("添加主任务:{}",mainTask);
        mainTaskService.save(mainTask);

        return R.success("添加任务成功");
    }
    /**
     * 查询主任务
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "mainTask", key = "#name+'_'+'mainTask'")
    public R<Page> page(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<MainTask> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<MainTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),MainTask::getName,name);
        queryWrapper.eq(status != null,MainTask::getStatus,status);
        queryWrapper.eq(fraction != null,MainTask::getFraction,fraction);
        queryWrapper.eq(categoryId != null,MainTask::getCategoryId,categoryId);
        queryWrapper.orderByDesc(MainTask::getCreateTime);
        mainTaskService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }

    /**
     * 查询主任务通过状态
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page/status")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<Page> pageByStatus(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<MainTask> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<MainTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),MainTask::getName,name);
        queryWrapper.eq(status != null,MainTask::getStatus,status);
        queryWrapper.eq(fraction != null,MainTask::getFraction,fraction);
        queryWrapper.eq(categoryId != null,MainTask::getCategoryId,categoryId);
        queryWrapper.orderByDesc(MainTask::getCreateTime);
        mainTaskService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }

    /**
     * 查询主任务通过积分
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page/fraction")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<Page> pageByFraction(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<MainTask> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<MainTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),MainTask::getName,name);
        queryWrapper.eq(status != null,MainTask::getStatus,status);
        queryWrapper.eq(fraction != null,MainTask::getFraction,fraction);
        queryWrapper.eq(categoryId != null,MainTask::getCategoryId,categoryId);
        queryWrapper.orderByDesc(MainTask::getCreateTime);
        mainTaskService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }

    /**
     * 微信小程序主任务分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize, String name){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<MainTask> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<MainTask> queryWrapper = new LambdaQueryWrapper();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name),MainTask::getName,name);

        //添加排序条件
        queryWrapper.orderByDesc(MainTask::getUpdateTime);

        List<MainTask> lists = new ArrayList<>();
        lists = mainTaskService.list(queryWrapper);
        for(MainTask list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);

        //执行查询
        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }


    /**
     * 定时发布任务
     * @return
     */
    @Scheduled(cron = "0 0 10,14,16 * * ?")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<Page> releaseByTime(){
        log.info("定时发布任务");

        Page<MainTask> pageInfo = new Page<>();
        LambdaUpdateWrapper<MainTask> updateWrapper = new LambdaUpdateWrapper<>();
        LocalDateTime date = LocalDateTime.now();
        log.info("当前时间:{}",date);
        updateWrapper.le(MainTask::getBeginTime,date);
        updateWrapper.eq(MainTask::getStatus,0);
        updateWrapper.set(MainTask::getStatus, 1);
        mainTaskService.update(updateWrapper);


        mainTaskService.page(pageInfo,updateWrapper);

        return R.success(pageInfo);
    }


    /**
     * 定时结束任务
     * @return
     */
    @Scheduled(cron = "0 0 10,14,16 * * ?")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<Page> endByTime(){
        log.info("定时完成任务");

        Page<MainTask> pageInfo = new Page<>();
        LambdaUpdateWrapper<MainTask> updateWrapper = new LambdaUpdateWrapper<>();
        LocalDateTime date = LocalDateTime.now();
        log.info("当前时间:{}",date);

        updateWrapper.le(MainTask::getEndTime,date);
        updateWrapper.eq(MainTask::getStatus,1);
        updateWrapper.set(MainTask::getStatus,2);
        mainTaskService.update(updateWrapper);


        mainTaskService.page(pageInfo,updateWrapper);

        return R.success(pageInfo);
    }




    /**
     * 任务发布
     * @param id
     * @return
     */
    @PostMapping("/release")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<String> release(@RequestParam("id") List<Long> id){
        log.info("发布主任务:{}", id);
        if (id == null) {
            return R.error("id不能为空");
        }
        LambdaUpdateWrapper<MainTask> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(MainTask::getStatus,1);
        updateWrapper.in(MainTask::getId,id);
        mainTaskService.update(updateWrapper);
        return R.success("发布成功");
    }

    /**
     * 删除主任务
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<String> delete(@RequestParam("id") List<Long> id){
        log.info("删除主任务:{}", id);
        if (id == null) {
            return R.error("id不能为空");
        }
        mainTaskService.removeByIds(id);
        return R.success("删除成功");
    }

    /**
     * 根据id修改任务（回显）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<MainTask> updateById(@PathVariable Long id){
        log.info("更新主任务:{}",id);
        MainTask mainTask = mainTaskService.getById(id);
        aliOSSUtils.refreshFile(mainTask.getImage());
        if(mainTask != null){
            return R.success(mainTask);
        }
        return R.error("未找到相应任务");
    }

    /**
     * 更新
     * @param mainTask
     * @return
     */
    @PostMapping("/update")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<String> update(@RequestBody MainTask mainTask){
        log.info("更新主任务:{}",mainTask);
        mainTaskService.updateById(mainTask);
        return R.success("更新成功");
    }

    /**
     * 微信端通过id查询任务信息
     * @param id
     * @return
     */
    @PostMapping("/select")
    public R<MainTask> selectById(Long id) {
        MainTask main = mainTaskService.getById(id);
        String url = aliOSSUtils.getFileUrl(main.getImage());
        main.setImage(url);
        if(id == null || main == null){
            return R.error("未找到相关数据");
        }
        return R.success(main);
    }


    /**
     * 查询共有主任务个数
     * @return
     */
    @PostMapping("/count")
    @CacheEvict(value = "mainTask",allEntries = true)
    public R<Integer> selectAll(){
        log.info("查询共有商品个数");
        int count = mainTaskService.count();
        return R.success(count);
    }


    /**
     * 查找分数前十主任务
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/selectByMain")
    public R<Page> selectByMain(int page, int pageSize){

        Page<MainTask> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<MainTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainTask::getStatus,1);
        queryWrapper.orderByDesc(MainTask::getFraction);
        queryWrapper.orderByDesc(MainTask::getCreateTime);

        mainTaskService.page(pageInfo,queryWrapper);


        return R.success(pageInfo);
    }





}
