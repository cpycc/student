package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Shopping;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.entity.User;
import com.example.student_x_c.entity.UserShopping;
import com.example.student_x_c.service.ShoppingService;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.service.UserShoppingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 学生兑换商品表
 */
@RestController
@RequestMapping("/usershopping")
@Slf4j
public class UserShoppingController {

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserShoppingService userShoppingService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;



    /**
     * 通过id兑换商品
     * @param     userShopping)
     * @return
     */
    @PostMapping("/submitById")
    @Transactional
    @CacheEvict(value = "shopping",allEntries = true)
    public R<String> submitById(@RequestBody UserShopping userShopping) {


        //使用redis锁解决多并发问题
        String lockkey = "lock_shopping_" + userShopping.getShoppingId();



        try {
            boolean result = Boolean.TRUE.equals(stringRedisTemplate.opsForValue().setIfAbsent(lockkey, "lock", 30, TimeUnit.SECONDS));
            if(!result){
                result = deductStockCAS(lockkey,3);
                if(!result){
                    return R.error("商品不足");
                }
            }
            Shopping shop = shoppingService.getById(userShopping.getShoppingId());
            User user = userService.getById(userShopping.getUserId());

            LambdaUpdateWrapper<Shopping> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(Shopping::getId, userShopping.getShoppingId());
            updateWrapper.set(Shopping::getNumber, shop.getNumber() - 1);


            LambdaUpdateWrapper<User> updateWrapper1 = new LambdaUpdateWrapper<>();
            updateWrapper1.eq(User::getId, userShopping.getUserId());
            updateWrapper1.set(User::getTask, user.getTask() - shop.getPrice());

            if (user.getTask() < shop.getPrice()) {
                return R.error("积分不足");
            }
            if (shop.getNumber() <= 0) {
                return R.error("商品不足");
            }


            userShoppingService.save(userShopping);
            shoppingService.update(updateWrapper);
            userService.update(updateWrapper1);

            return R.success("兑换成功");
        }finally {
            //释放锁
            stringRedisTemplate.delete(lockkey);
        }
    }

    /**
     * 设置尝试次数
     * @param lockkey
     * @param count
     * @return
     */
    public boolean deductStockCAS(String lockkey,Integer count){
        try {
            int i=0;
            do{
                Thread.sleep(1000L);
                i++;
                if(i == count +1){
                    return false;
                }
            }while (Boolean.FALSE.equals(stringRedisTemplate.opsForValue().setIfAbsent(lockkey, "lock", 30, TimeUnit.SECONDS)));
            return true;
        }catch (Exception e){
            return false;
        }
}


    /**
     * 管理端通过用户id分页查询兑换的商品
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page")
    public R<Page> page(int page, int pageSize, Long id){
        Page<UserShopping> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<UserShopping> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserShopping::getUserId,id);
        queryWrapper.orderByDesc(UserShopping::getUpdateTime);

        userShoppingService.page(pageInfo,queryWrapper);
        return R.success(pageInfo);
    }





}
