package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.service.SocietiesService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 社团表
 */
@Slf4j
@RestController
@RequestMapping("/societies")
public class SocietiesController {

    @Autowired
    private SocietiesService societiesService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 新增社团
     * @param societies
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "societies",allEntries = true)
    public R<String> add(@RequestBody Societies societies){
        societiesService.save(societies);
        return R.success("新增社团成功");
    }

    /**
     * 管理端社团分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "societies", key = "#name+'_'+'societies'")
    public R<Page> page(int page, int pageSize, String name){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<Societies> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<Societies> queryWrapper = new LambdaQueryWrapper();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name),Societies::getName,name);
        //添加排序条件
        queryWrapper.orderByDesc(Societies::getUpdateTime);
        //执行查询
        societiesService.page(pageInfo,queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 微信小程序社团分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize, String name){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<Societies> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<Societies> queryWrapper = new LambdaQueryWrapper();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name),Societies::getName,name);
        //添加排序条件
        queryWrapper.orderByDesc(Societies::getUpdateTime);

        List<Societies> lists = new ArrayList<>();
        lists = societiesService.list(queryWrapper);
        for(Societies list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);

        //执行查询
        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }




    /**
     * 回传社团信息（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Societies> update(@PathVariable Long id){
        Societies societies = societiesService.getById(id);
        aliOSSUtils.refreshFile(societies.getImage());
        return R.success(societies);
    }

    /**
     * 更新
     * @param societies
     * @return
     */
    @PostMapping("/update")
    @CacheEvict(value = "societies",allEntries = true)
    public R<String> update(@RequestBody Societies societies){
        log.info("更新主任务:{}",societies);
        societiesService.updateById(societies);
        return R.success("更新成功");
    }

    /**
     * 删除社团
     * @param ids
     * @return
     */
    @DeleteMapping ("/delete")
    @CacheEvict(value = "societies",allEntries = true)
    public R<String> delete(@RequestParam("id") List<Long> ids){
        societiesService.removeByIds(ids);
        return R.success("删除社团成功");
    }

    /**
     * 热门社团
     * @param page
     * @param pageSize
     * @param
     * @return
     */
    @PostMapping("/pageR")
    @CacheEvict(value = "societies",allEntries = true)
    public R<Page> pageR(int page, int pageSize){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<Societies> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<Societies> queryWrapper = new LambdaQueryWrapper();

        queryWrapper.gt(Societies::getNumber,50);

        //添加排序条件
        queryWrapper.orderByDesc(Societies::getUpdateTime);

        List<Societies> lists = new ArrayList<>();
        lists = societiesService.list(queryWrapper);
        for(Societies list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);


        pageInfo.setRecords(lists);

        return R.success(pageInfo);
    }


    /**
     * 查询共有商品个数
     * @return
     */
    @PostMapping("/count")
    @CacheEvict(value = "societies",allEntries = true)
    public R<Integer> selectAll(){
        log.info("查询共有商品个数");
        int count = societiesService.count();
        return R.success(count);
    }
}
