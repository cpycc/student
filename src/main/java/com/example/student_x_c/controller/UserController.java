package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.constant.JwtClaimsConstant;
import com.example.student_x_c.dto.PermissionDto;
import com.example.student_x_c.entity.*;
import com.example.student_x_c.service.MenuService;
import com.example.student_x_c.service.RoleService;
import com.example.student_x_c.service.UserRoleService;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.utils.AliOSSUtils;
import com.example.student_x_c.utils.JwtUtils;
import com.example.student_x_c.utils.SMSUtils;
import com.example.student_x_c.utils.ValidateCodeUtils;
import com.example.student_x_c.vo.MenuVO;
import com.example.student_x_c.vo.UserVO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProperties jwtProperties;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    @Autowired
    private MenuService menuService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;


//    /**
//     * 登录
//     *
//     * @param session
//     * @param user
//     * @return
//     */
//    @PostMapping("/login")
//    public R<User> login(HttpSession session, @RequestBody User user) {
//        log.info("user：{}", user);
//        //将页面提交的密码进行md5处理
//        String password = user.getPassword();
//        password = DigestUtils.md5DigestAsHex(password.getBytes());
//
//
//        //根据用户名查询数据库
//        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
//        queryWrapper.eq(User::getName, user.getName());
//        User user1 = userService.getOne(queryWrapper);
//
//        //如果没有登录失败
//        if (user1 == null) {
//            return R.error("登录失败");
//        }
//
//        //密码对比不是返回失败
//        if (!user1.getPassword().equals(password)) {
//            return R.error("登陆失败");
//        }
//        //登录成功
//        String key = "user_" + user1.getId() + "_c";
//        redisTemplate.opsForValue().set(key, user1);
//        session.setAttribute("user", user1.getId());
//        return R.success(user1);
//    }
    /**
     * 登录
     *
     * @param session
     * @param user
     * @return
     */
    @PostMapping("/login")
    public R<UserVO> login(HttpSession session, @RequestBody User user) {
        log.info("user：{}", user);
        //将页面提交的密码进行md5处理
        String password = user.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());


        //根据用户名查询数据库
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getName, user.getName());
        User user1 = userService.getOne(queryWrapper);

        //查询用户对应的菜单权限
        List<PermissionDto> permissions = userService.getUserPermission(user1.getId());

        //如果没有登录失败
        if (user1 == null) {
            return R.error("登录失败");
        }

        if(user1.getStatus() == 0){
            return R.error("账号被禁用");
        }

        //密码对比不是返回失败
        if (!user1.getPassword().equals(password)) {
            return R.error("密码或用户名错误，请重新提交登录");
        }
        //登录成功
        String key = "user_" + user1.getId() + "_c";
        redisTemplate.opsForValue().set(key, user1);

        //登录成功后生成jwt令牌
        Map<String,Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID,user1.getId());
        String token = JwtUtils.createJWT(jwtProperties.getAdminSecretKey(),jwtProperties.getAdminTtl(),claims);

        UserVO userVO = new UserVO.Builder()
                .id(user1.getId())
                .name(user1.getName())
                .roles(user1.getRole())
                .permissions(permissions)
                .token(token)
                .build();

        return R.success(userVO);
    }

    /**
     * 判断角色接口
     *
     * @param request
     * @return
     */
    @PostMapping("/getInfo")
    public R<UserVO> getInfo(HttpServletRequest request) {

        String token = request.getHeader("Authorization");
        if (token!= null && token.startsWith("Bearer ")) {
             token = token.substring("Bearer ".length()).trim();
            // 现在token变量只包含实际的token字符串
        }


        try {
            // 使用相同的密钥来解码，确保与生成JWT时使用的密钥一致
            Claims claims = JwtUtils.parseJWT(jwtProperties.getAdminSecretKey(), token);


            // 现在你可以访问claims中的数据
            Long userId = claims.get(JwtClaimsConstant.USER_ID, Long.class);
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getId,userId);
            User user1 = userService.getOne(queryWrapper);

            //查询用户对应的菜单权限
            List<PermissionDto> permissions = userService.getUserPermission(user1.getId());

            UserVO userVO = new UserVO.Builder()
                    .id(user1.getId())
                    .name(user1.getName())
                    .roles(user1.getRole())
                    .permissions(permissions)
                    .token(token)
                    .build();

            return R.success(userVO);

        } catch (Exception e) {
            // 处理解码失败的情况，如过期、篡改等
            System.err.println("Error decoding JWT: " + e.getMessage());
        }

        return R.error("身份失效");
    }

    /**
     * 获取菜单
     *
     * @param request
     * @return
     */
    @PostMapping("/getMenu")
    @Transactional
    public R<List<MenuVO>> getMenu(HttpServletRequest request) {

        String token = request.getHeader("Authorization");
        if (token!= null && token.startsWith("Bearer ")) {
            token = token.substring("Bearer ".length()).trim();
            // 现在token变量只包含实际的token字符串
        }

        try {
            // 使用相同的密钥来解码，确保与生成JWT时使用的密钥一致
            Claims claims = JwtUtils.parseJWT(jwtProperties.getAdminSecretKey(), token);

            // 现在你可以访问claims中的数据k
            Long userId = claims.get(JwtClaimsConstant.USER_ID, Long.class);
            List<MenuVO> menuVOS = new ArrayList<>();
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getId,userId);
            User user1 = userService.getOne(queryWrapper);

            //查询用户对应的菜单权限
            List<PermissionDto> permissions = userService.getUserPermission(user1.getId());

            for(PermissionDto permissionDto : permissions){
                LambdaQueryWrapper<Menu> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(Menu::getId,permissionDto.getId());
                Menu menu = menuService.getOne(queryWrapper1);


                MenuVO menuVO = new MenuVO();
                menuVO.setName(menu.getName());
                menuVO.setPath(menu.getPath());
                menuVO.setComponent(menu.getComponent());
                menuVO.setHidden(menu.getHidden());

                MenuVO.Meta meta = new MenuVO.Meta();
                meta.setTitle(menu.getTitle());
                meta.setIcon(menu.getIcon());
                meta.setNoCache(menu.getNoCache());
                meta.setRedirect(menu.getRedirect());

                menuVO.setMeta(meta);


                log.info("meunVO:{}",menuVO);

                menuVOS.add(menuVO);
            }

            return R.success(menuVOS);

        } catch (Exception e) {
            // 处理解码失败的情况，如过期、篡改等
            System.err.println("Error decoding JWT: " + e.getMessage());
        }

        return R.error("身份失效");
    }








    /**
     * 学生注册
     *
     * @param user
     * @return
     */
    @PostMapping("/regist")
    @Transactional
    public R<String> regist(@RequestBody User user) {
        log.info("user：{}", user);
        //将页面提交的密码进行md5处理
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        user.setStatus(0);

        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getCode,user.getRole());
        Role role = roleService.getOne(queryWrapper);



        userService.save(user);

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());

        userRoleService.save(userRole);
        String key = "user_" + user.getId() + "_c";
        redisTemplate.opsForValue().set(key, user, 30, TimeUnit.MINUTES);
        return R.success("注册成功");
    }

    /**
     * 社团团长注册
     * @param user
     * @return
     */
    @PostMapping("/regists")
    public R<String> regists(@RequestBody User user){

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(User::getPhone,user.getPhone());
        queryWrapper.eq(User::getPassword,DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        queryWrapper.eq(User::getAcademyName,user.getAcademyName());
        queryWrapper.eq(User::getName,user.getName());

        if(userService.getMap(queryWrapper) != null){
            queryWrapper.eq(User::getRole,user.getRole());
            if(userService.getMap(queryWrapper) != null){
                return R.error("该社团团长已存在");
            }
            user.setRole("translate");
            user.setStatus(0);
            user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));

            LambdaQueryWrapper<Role> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(Role::getCode,user.getRole());
            Role role = roleService.getOne(queryWrapper1);



            userService.save(user);

            UserRole userRole = new UserRole();
            userRole.setUserId(user.getId());
            userRole.setRoleId(role.getId());
            userRoleService.save(userRole);
            return R.success("注册成功");
        }
        return R.error("注册失败");
    }

    /**
     * 学生分页查询（条件为姓名）
     *
     * @param name
     * @return
     */
    @PostMapping("/query")
    @Cacheable(value = "user", key = "#name+'_'+#phone")
    public R<Page> queryByName(int page, int pageSize, String name, String phone, Integer status, String role) {
        log.info("page:{},pageSize:{},userId:{}", page, pageSize, name);
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), User::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(phone), User::getPhone, phone);
        queryWrapper.like(!ObjectUtils.isEmpty(status), User::getStatus, status);
        queryWrapper.like(StringUtils.isNotEmpty(role), User::getRole, role);
        queryWrapper.orderByDesc(User::getId);

        userService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }


    /**
     * 学生分页查询通过状态（条件为姓名）
     *
     * @param name
     * @return
     */
    @PostMapping("/query/status")
    @CacheEvict(value = "user",allEntries = true)
    public R<Page> queryByStatus(int page, int pageSize, String name, String phone, Integer status, String role) {
        log.info("page:{},pageSize:{},userId:{}", page, pageSize, name);
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), User::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(phone), User::getPhone, phone);
        queryWrapper.like(!ObjectUtils.isEmpty(status), User::getStatus, status);
        queryWrapper.like(StringUtils.isNotEmpty(role), User::getRole, role);
        queryWrapper.orderByDesc(User::getId);

        userService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 学生分页查询通过角色（条件为姓名）
     *
     * @param name
     * @return
     */
    @PostMapping("/query/role")
    @CacheEvict(value = "user",allEntries = true)
    public R<Page> queryByStatusByRole(int page, int pageSize, String name, String phone, Integer status, String role) {
        log.info("page:{},pageSize:{},userId:{}", page, pageSize, name);
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), User::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(phone), User::getPhone, phone);
        queryWrapper.like(!ObjectUtils.isEmpty(status), User::getStatus, status);
        queryWrapper.like(StringUtils.isNotEmpty(role), User::getRole, role);
        queryWrapper.orderByDesc(User::getId);

        userService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }





    /**
     * 退出登录
     *
     * @param userId
     * @return
     */
    @PostMapping("/logout")
    public R<String> logout(String userId) {
        log.info("userId:{}", userId);
        redisTemplate.delete("user_" + userId + "_c");
        return R.success("注销成功");
    }

    /**
     * 通过id进行查询(回显)
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<User> selectById(@PathVariable Long id) {
        log.info("id:{}", id);
        User user = userService.getById(id);
        aliOSSUtils.refreshFile(user.getAvatar());
        log.info("user:{}", user);
        if (user != null) {
            return R.success(user);
        }
        return R.error("查询失败");
    }

    /**
     * 微信端回显
     * @param id
     * @return
     */
    @PostMapping("/selectWx")
    public R<User> selectWx(@RequestParam("id") Long id){
        User user = userService.getById(id);
        return R.success(user);
    }



    /**
     * 发送短信
     *
     * @param user
     * @return
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        //1.获取手机号
        String phone = user.getPhone();

        if (StringUtils.isNotEmpty(phone)) {
            //2.生成4位数验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code={}", code);

            //3.调用阿里云提供的短信服务api
            SMSUtils.sendMessage("小陈瑞吉外卖", "SMS_464060619", phone, code);
            //4.保存生成的验证码Session
            //session.setAttribute(phone,code);

            //4.将生成的验证码放到Rides中，并且设置时间
            redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);

            return R.success("短信发送成功");
        }

        return R.error("短信发送失败");
    }

    /**
     * 手机号登录
     *
     * @param phone
     * @return
     */
    @PostMapping("/loginByPhone")
    public R<User> loginByPhone(String phone, String code) {
        log.info("user:{}", phone, code);
        //获取手机号，验证码
//         phone = phone.toString();
//         code = code.toString();
        //从redis中获取缓存的验证码
        Object codeInSession = redisTemplate.opsForValue().get(phone);


        if (codeInSession != null && codeInSession.equals(code)) {
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone, phone);

            User user = userService.getOne(queryWrapper);
            //如果是新用户，保存用户信息到数据库，自动完成注册
            if (user == null) {
                return R.error("用户未注册,请前往注册");
            }
            //如果用户登陆成功删除验证码
            redisTemplate.delete(phone);
            return R.success(user);
        }
        return R.error("登录失败");
    }

    /**
     * 学生添加
     *
     * @param user
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> add(@RequestBody User user) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getName,user.getName());
        queryWrapper.eq(User::getPhone,user.getPhone());
        User one = userService.getOne(queryWrapper);
        if(one!=null){
            return R.error("用户已存在");
        }

        user.setAvatar("https://tse2-mm.cn.bing.net/th/id/OIP-C.ZcjidB6ytMyNAjg9clT4PAHaNK?rs=1&pid=ImgDetMain");
        user.setTask(0);
        userService.save(user);
        return R.success("添加学生成功");
    }

    /**
     * 更新
     *
     * @param user
     * @return
     */
    @PostMapping("/update")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> update(@RequestBody User user) {
        log.info("更新:{}", user);

        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getCode, user.getRole());
        Role role = roleService.getOne(queryWrapper);

        LambdaQueryWrapper<UserRole> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(UserRole::getUserId, user.getId());
        if (userRoleService.getOne(queryWrapper1) != null) {
            userRoleService.remove(queryWrapper1);
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getId());
            userRole.setRoleId(role.getId());
            userRoleService.save(userRole);
        }
        userService.updateById(user);
        return R.success("更新成功");
    }


    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> delete(@RequestParam("id") List<Long> id) {
        log.info("删除用户:{}", id);
        if (id == null) {
            return R.error("id不能为空");
        }
        userService.removesByIds(id);
        return R.success("删除成功");
    }

    /**
     * 更改用户状态
     *
     * @return
     */
    @GetMapping("/updateById/{id}")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> updateById(@PathVariable Long id) {

        userService.updatesById(id);
        return R.success("更新成功");

    }

    /**
     * 重置密码
     *
     * @param id
     * @return
     */
    @PutMapping("/updatePassWord/{id}")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> updatePassWord(@PathVariable Long id) {
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getId, id);
        String password = "123456";
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        updateWrapper.set(User::getPassword,password);

        if (userService.update(updateWrapper)) {
            return R.success("更新成功");
        }

        return R.error("更新失败");

    }

    /**
     * 获取用户积分
     * @param id
     * @return
     */
    @GetMapping("/getTask")
    public R<Integer> getTask(@PathVariable Long id) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId,id);
        return R.success(userService.getOne(queryWrapper).getTask());
    }




    /**
     * 修改头像
     * @return
     * @throws Exception
     */
    @PostMapping("/upload")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> upload(@RequestParam("file") MultipartFile avatar) throws Exception {



        //调用工具类方法将文件存入阿里云OSS
        String fileName = aliOSSUtils.upload(avatar);

        // 设置响应头
        String url = aliOSSUtils.getOrReloadImage(fileName);

        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
//        updateWrapper.eq(User::getId, id);
        updateWrapper.set(User::getAvatar, fileName);
        userService.update(updateWrapper);

        return R.success(url);
    }

    /**
     * 修改密码
     *
     * @param id
     * @return
     */
    @PutMapping("/updatePass")
    @CacheEvict(value = "user",allEntries = true)
    public R<String> updatePass(String oldPassword,String newPassword,Long id) {
        log.info("修改密码:{}",oldPassword);
        log.info("id:{}",id);
        if(oldPassword == null || newPassword == null){
            return R.error("密码不能为空");
        }

        oldPassword = DigestUtils.md5DigestAsHex(oldPassword.getBytes());
        newPassword = DigestUtils.md5DigestAsHex(newPassword.getBytes());


        if(oldPassword.equals(newPassword)){
            return R.error("新密码不能与旧密码相同");
        }

        //判断旧密码是否正确
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId,id);
        queryWrapper.eq(User::getPassword,oldPassword);
        User one = userService.getOne(queryWrapper);
        if(one == null){
            return R.error("旧密码错误");
        }

        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getId,id);
        updateWrapper.set(User::getPassword,newPassword);

        userService.update(updateWrapper);

        return R.success("更改成功");



    }


    /**
     * 排行榜接口
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/rankings")
    public R<Page> rankings(int page, int pageSize){
        Page<User> pageInfo = new Page(page, pageSize);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.orderByDesc(User::getTask);

        List<User> lists = userService.list(queryWrapper);
        for(User list:lists){
            list.setAvatar(aliOSSUtils.getFileUrl(list.getAvatar()));
        }

        pageInfo.setRecords(lists);

        return R.success(pageInfo);
    }

    /**
     * 查询用户
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/selectByUser")
    public R<Page> selectByUser(int page, int pageSize) {

        Page<User> pageInfo = new Page(page, pageSize);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(User::getTask);
        queryWrapper.orderByDesc(User::getCreateTime);

        userService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);

    }


    /**
     * 获取用户权限
     * @param userId
     * @return
     */
    @GetMapping("/permission/{userId}")
    public R<List<PermissionDto>> getUserPermission(@PathVariable Long userId) {
        return R.success(userService.getUserPermission(userId));
    }

    /**
     * 获取用户角色
     * @param userId
     * @return
     */
    @GetMapping("/roles/{userId}")
    public R<List<Role>> getUserRoles(@PathVariable Long userId) {
        return R.success(userService.getUserRoles(userId));
    }


    /**
     * 保存用户角色
     * @param userId
     * @param ids
     * @return
     */
    @PostMapping("/roles/{userId}")
    public R<Boolean> saveUserRoles(@PathVariable Long userId, @RequestBody Set<Long> ids) {
        return R.success(userService.saveUserRoles(userId, ids));
    }



}
