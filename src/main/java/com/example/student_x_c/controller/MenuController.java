package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Menu;
import com.example.student_x_c.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * 新增菜单
     * @param menu
     * @return
     */
    @PostMapping("/add")
    public R<String> add(@RequestBody Menu menu){
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Menu::getTitle,menu.getTitle());
        queryWrapper.eq(Menu::getPId,menu.getPId());
        Menu one = menuService.getOne(queryWrapper);
        if(one!=null){
            return R.error("不能重复添加");
        }

        menuService.save(menu);
        return R.success("角色添加成功");
    }

    /**
     * 删除菜单
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    public R<String> delete(@RequestParam("ids") List<Long> ids){
        if (ids == null) {
            return R.error("id不能为空");
        }
        menuService.removeByIds(ids);
        return R.success("删除成功");
    }

    /**
     * 通过id进行查询(回显)
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Menu> selectById(@PathVariable Long id) {
        log.info("id:{}", id);
        Menu menu = menuService.getById(id);
        if (menu != null) {
            return R.success(menu);
        }
        return R.error("查询失败");
    }

    /**
     * 更新
     *
     * @param menu
     * @return
     */
    @PostMapping("/update")
    public R<String> update(@RequestBody Menu menu) {
        log.info("更新:{}", menu);
        menuService.update(menu);
        return R.success("更新成功");
    }


    /**
     * 查询菜单
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/list")
    public R<Page> list(int page, int pageSize, String name, LocalDateTime createTime,LocalDateTime updateTime){
        log.info("page:{}",page);
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Menu::getTitle, name);
        queryWrapper.between(createTime!=null,Menu::getCreateTime,createTime,updateTime);
        queryWrapper.orderByAsc(Menu::getSort);
        menuService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);

    }


    /**
     * 查询enable为0菜单
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/listByEnable")
    public R<Page> listByEnable(int page, int pageSize){
        log.info("page:{}",page);
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Menu::getEnable, 1);
        menuService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }
}
