package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Employee;

import com.example.student_x_c.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/ace")
@Slf4j
public class AceController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private RedisTemplate redisTemplate;



    /**
     * 公告添加
     * @param employee
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "Ace",allEntries = true)
    public R<String> add(@RequestBody Employee employee) {
        employeeService.save(employee);
        return R.success("新增公告成功");
    }

    /**
     * 通过id查询公告(回显)
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Employee> selectById(@PathVariable Long id) {
        if(id == null){
            return R.error("id不能为空");
        }
        Employee employee = employeeService.getById(id);
        if(employee == null){
            return R.error("没有查询到对应员工");
        }
        return R.success(employee);
    }

    /**
     * 更新
     * @param employee
     * @return
     */
    @PutMapping("/update")
    @CacheEvict(value = "Ace",allEntries = true)
    public R<String> update(@RequestBody Employee employee){
        log.info("更新:{}",employee);

        employeeService.updateById(employee);

        return R.success("更新成功");
    }


    /**
     * 删除公告
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "Ace",allEntries = true)
    public R<String> delete(@RequestParam("ids") List<Long> id){
        log.info("删除主任务:{}",id);
        if(id == null){
            return R.error("id不能为空");
        }
        employeeService.removeByIds(id);
        return R.success("删除成功");
    }


    /**
     * 分页查询用户列表
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "Ace", key = "#name+'_'+'Ace'")
    public R<Page> page(int page, int pageSize, String name,Integer status) {
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
        queryWrapper.eq(status != null, Employee::getStatus, status);
        queryWrapper.orderByAsc(Employee::getUpdateTime);
        employeeService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 分页查询用户列表
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page/status")
    @CacheEvict(value = "Ace",allEntries = true)
    public R<Page> pageByStatus(int page, int pageSize, String name,Integer status) {
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
        queryWrapper.eq(status != null, Employee::getStatus, status);
        queryWrapper.orderByAsc(Employee::getUpdateTime);
        employeeService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }






}
