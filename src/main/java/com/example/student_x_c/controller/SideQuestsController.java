package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.JacksonObjectMapper;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.MainTask;
import com.example.student_x_c.entity.SideQuests;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.service.SideQuestsService;
import com.example.student_x_c.utils.AliOSSUtils;
import javafx.geometry.Side;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/side")
public class SideQuestsController {

    @Autowired
    private SideQuestsService sideQuestsService;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 添加支线任务
     * @param sideQuests
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "side",allEntries = true)
    public R<String> add(@RequestBody SideQuests sideQuests){
        log.info("添加主任务:{}",sideQuests);

        sideQuestsService.save(sideQuests);

        return R.success("添加支线任务成功");
    }

    /**
     * 查询支线任务
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "side", key = "#name+'_'+#fraction")
    public R<Page> page(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<SideQuests> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<SideQuests> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),SideQuests::getName,name);
        queryWrapper.eq(status != null,SideQuests::getStatus,status);
        queryWrapper.eq(fraction != null,SideQuests::getFraction,fraction);
        queryWrapper.eq(categoryId != null,SideQuests::getCategoryId,categoryId);
        queryWrapper.orderByDesc(SideQuests::getCreateTime);
        sideQuestsService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }

    /**
     * 查询支线任务通过status（解决缓存问题）
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page/status")
    @CacheEvict(value = "side",allEntries = true)
    public R<Page> pageByStatus(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<SideQuests> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<SideQuests> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),SideQuests::getName,name);
        queryWrapper.eq(status != null,SideQuests::getStatus,status);
        queryWrapper.eq(fraction != null,SideQuests::getFraction,fraction);
        queryWrapper.eq(categoryId != null,SideQuests::getCategoryId,categoryId);
        queryWrapper.orderByDesc(SideQuests::getCreateTime);
        sideQuestsService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }


    /**
     * 查询支线任务通过fraction（解决缓存问题）
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page/fraction")
    @CacheEvict(value = "side",allEntries = true)
    public R<Page> pageByFraction(int page, int pageSize, String name, Integer status,Integer fraction,Integer categoryId){
        log.info("page={},pageSize={},name={}",page,pageSize,name);
        Page<SideQuests> pageInfo = new Page<>(page,pageSize);
        LambdaQueryWrapper<SideQuests> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),SideQuests::getName,name);
        queryWrapper.eq(status != null,SideQuests::getStatus,status);
        queryWrapper.eq(fraction != null,SideQuests::getFraction,fraction);
        queryWrapper.eq(categoryId != null,SideQuests::getCategoryId,categoryId);
        queryWrapper.orderByDesc(SideQuests::getCreateTime);
        sideQuestsService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);

    }


    /**
     * 任务发布
     * @param id
     * @return
     */
    @PostMapping("/release")
    @CacheEvict(value = "side",allEntries = true)
    public R<String> release(@RequestParam("id") List<Long> id){
        log.info("发布主任务:{}", id);
        if (id == null) {
            return R.error("id不能为空");
        }
        LambdaUpdateWrapper<SideQuests> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(SideQuests::getStatus,1);
        updateWrapper.in(SideQuests::getId,id);
        sideQuestsService.update(updateWrapper);
        return R.success("发布成功");
    }

    /**
     * 微信小程序社团分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize, String name){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<SideQuests> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<SideQuests> queryWrapper = new LambdaQueryWrapper();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name),SideQuests::getName,name);
        //添加排序条件
        queryWrapper.orderByDesc(SideQuests::getUpdateTime);

        List<SideQuests> lists = new ArrayList<>();
        lists = sideQuestsService.list(queryWrapper);
        for(SideQuests list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);

        //执行查询
        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }

    /**
     * 删除支线任务
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "side",allEntries = true)
    public R<String> delete(Long id){
        log.info("删除主任务:{}",id);
        SideQuests sideQuests = sideQuestsService.getById(id);
        if(sideQuests == null){
            return R.error("主任务不存在");
        }
        sideQuestsService.removeById(id);
        return R.success("删除成功");
    }

    /**
     * 根据id修改任务
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<SideQuests> updateById(@PathVariable Long id){
        log.info("支线任务:{}",id);
        SideQuests sideQuests = sideQuestsService.getById(id);
        aliOSSUtils.refreshFile(sideQuests.getImage());
        if(sideQuests != null){
            return R.success(sideQuests);
        }
        return R.error("未找到相应任务");
    }

    /**
     * 更新
     * @param sideQuests
     * @return
     */
    @CacheEvict(value = "side",allEntries = true)
    @PostMapping("update")
    public R<String> update(@RequestBody SideQuests sideQuests){
        log.info("更新主任务:{}",sideQuests);
        sideQuestsService.updateById(sideQuests);
        return R.success("更新成功");
    }

    /**
     * 微信端通过id查询任务信息
     * @param id
     * @return
     */
    @PostMapping("/select")
    public R<SideQuests> selectById(Long id) {
        SideQuests sid = sideQuestsService.getById(id);
        String url = aliOSSUtils.getFileUrl(sid.getImage());
        sid.setImage(url);
        if(id == null || sid == null){
            return R.error("未找到相关数据");
        }
        return R.success(sid);
    }

    /**
     * 查询共有支线任务个数
     * @return
     */
    @PostMapping("/count")
    @CacheEvict(value = "side",allEntries = true)
    public R<Integer> selectAll(){
        log.info("查询共有商品个数");
        int count = sideQuestsService.count();
        return R.success(count);
    }


    /**
     * 定时发布任务
     * @return
     */
    @Scheduled(cron = "0 0 10,14,16 * * ?")
    @CacheEvict(value = "side",allEntries = true)
    public R<Page> releaseByTime(){
        log.info("定时发布任务");

        Page<SideQuests> pageInfo = new Page<>();
        LambdaUpdateWrapper<SideQuests> updateWrapper = new LambdaUpdateWrapper<>();
        LocalDateTime date = LocalDateTime.now();
        log.info("当前时间:{}",date);

        updateWrapper.le(SideQuests::getBeginTime,date);
        updateWrapper.eq(SideQuests::getStatus,0);
        updateWrapper.set(SideQuests::getStatus, 1);
        sideQuestsService.update(updateWrapper);



        sideQuestsService.page(pageInfo,updateWrapper);

        return R.success(pageInfo);
    }

    /**
     * 定时结束任务
     * @return
     */
    @Scheduled(cron = "0 0 10,14,16 * * ?")
    @CacheEvict(value = "side",allEntries = true)
    public R<Page> endByTime(){
        log.info("定时发布任务");

        Page<SideQuests> pageInfo = new Page<>();
        LambdaUpdateWrapper<SideQuests> updateWrapper = new LambdaUpdateWrapper<>();
        LocalDateTime date = LocalDateTime.now();
        log.info("当前时间:{}",date);

        updateWrapper.le(SideQuests::getEndTime,date);
        updateWrapper.eq(SideQuests::getStatus,1);
        updateWrapper.set(SideQuests::getStatus,2);
        sideQuestsService.update(updateWrapper);


        sideQuestsService.page(pageInfo,updateWrapper);

        return R.success(pageInfo);
    }

    /**
     * 查找分数前十支线任务
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/selectBySide")
    public R<Page> selectBySide(int page, int pageSize){

        Page<SideQuests> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<SideQuests> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SideQuests::getStatus,1);
        queryWrapper.orderByDesc(SideQuests::getFraction);
        queryWrapper.orderByDesc(SideQuests::getCreateTime);

        sideQuestsService.page(pageInfo,queryWrapper);


        return R.success(pageInfo);
    }


}
