package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Shopping;
import com.example.student_x_c.entity.SideQuests;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.service.ShoppingService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品设置
 */
@Slf4j
@RestController
@RequestMapping("/shopping")
public class ShoppingController {

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 新增商品
     * @param shopping
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "shopping",allEntries = true)
    public R<String> add(@RequestBody Shopping shopping){
        shoppingService.save(shopping);
        return R.success("新增成功");
    }

    /**
     * 通过id获得商品（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Shopping> updateById(@PathVariable Long id){
        Shopping shopping = shoppingService.getById(id);
        aliOSSUtils.refreshFile(shopping.getImage());
        return R.success(shopping);
    }


    /**
     * 微信通过id获得商品图片（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/wx/{id}")
    public R<Shopping> updateWxById(@PathVariable Long id){
        Shopping shopping = shoppingService.getById(id);
        shopping.setImage(aliOSSUtils.getFileUrl(shopping.getImage()));
        return R.success(shopping);
    }

    /**
     * 更新
     * @param shopping
     * @return
     */

    @PostMapping("/update")
    @CacheEvict(value = "shopping",allEntries = true)
    public R<String> update(@RequestBody Shopping shopping){
        log.info("更新主任务:{}",shopping);
        shoppingService.updateById(shopping);
        return R.success("更新成功");
    }

    /**
     * 分页查询商品
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "shopping", key = "#name+'_'+'shopping'")
    public R<Page> page(int page, int pageSize, String name, Integer status){
        Page<Shopping> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<Shopping> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),Shopping::getName,name);
        queryWrapper.eq(status != null,Shopping::getStatus,status);
        queryWrapper.orderByDesc(Shopping::getUpdateTime);

        shoppingService.page(pageInfo,queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 分页查询商品通过状态
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page/status")
    @CacheEvict(value = "shopping",allEntries = true)
    public R<Page> pageByStatus(int page, int pageSize, String name, Integer status){
        Page<Shopping> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<Shopping> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),Shopping::getName,name);
        queryWrapper.eq(status != null,Shopping::getStatus,status);
        queryWrapper.orderByDesc(Shopping::getUpdateTime);

        shoppingService.page(pageInfo,queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 微信小程序商品分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize, String name){
        log.info("分页查询,当前页码{},每页记录数{}",page,pageSize);
        //分页构造器
        Page<Shopping> pageInfo = new Page(page,pageSize);
        //条件构造器
        LambdaQueryWrapper<Shopping> queryWrapper = new LambdaQueryWrapper();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name),Shopping::getName,name);
        //添加排序条件
        queryWrapper.orderByDesc(Shopping::getUpdateTime);

        List<Shopping> lists = new ArrayList<>();
        lists = shoppingService.list(queryWrapper);
        for(Shopping list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);

        //执行查询
        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }




    /**
     * 批量删除商品
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "shopping",allEntries = true)
    public R<String> delete(@RequestParam("ids") List<Long> ids){
        shoppingService.removeByIds(ids);
        return R.success("删除成功");
    }

    /**
     * 查询共有商品个数
     * @return
     */
    @PostMapping("/count")
    @CacheEvict(value = "shopping",allEntries = true)
    public R<Integer> selectAll(){
        log.info("查询共有商品个数");
        int count = shoppingService.count();
        return R.success(count);
    }


    /**
     * 查找最好前十商品
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/selectByShopping")
    public R<Page> selectByShopping(int page, int pageSize){

        Page<Shopping> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<Shopping> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Shopping::getPrice);
        queryWrapper.orderByDesc(Shopping::getCreateTime);

        shoppingService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }





}
