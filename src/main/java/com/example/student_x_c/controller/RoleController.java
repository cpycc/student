package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Permission;
import com.example.student_x_c.entity.Role;
import com.example.student_x_c.entity.User;
import com.example.student_x_c.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 新增角色
     * @param role
     * @return
     */
    @PostMapping("/add")
    public R<String> add(@RequestBody Role role){
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getCode,role.getCode());
        queryWrapper.eq(Role::getName,role.getName());
        Role one = roleService.getOne(queryWrapper);

        if(one != null){
            return R.error("不能重复添加");
        }else{
            roleService.save(role);
        }
        return R.success("角色添加成功");
    }

    /**
     * 删除角色
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    public R<String> delete(@RequestParam("ids") List<Long> ids){
        if (ids == null) {
            return R.error("id不能为空");
        }
        roleService.removeByIds(ids);
        return R.success("删除成功");
    }

    /**
     * 通过id进行查询(回显)
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Role> selectById(@PathVariable Long id) {
        log.info("id:{}", id);
        Role role = roleService.getById(id);
        if (role != null) {
            return R.success(role);
        }
        return R.error("查询失败");
    }

    /**
     * 更新
     *
     * @param role
     * @return
     */
    @PostMapping("/update")
    public R<String> update(@RequestBody Role role) {
        log.info("更新:{}", role);
        roleService.updateById(role);
        return R.success("更新成功");
    }

    /**
     * 查询角色
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/list")
    public R<Page> list(int page, int pageSize, String name){
        Page pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Role::getName, name);
        roleService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);

    }


    /**
     * 获取角色权限
     * @param roleId
     * @return
     */
    @GetMapping("{roleId}/permission")
    public R<List<Permission>> getPermission(@PathVariable Long roleId) {
        return R.success(roleService.getPermission(roleId));
    }

    /**
     * 获取权限信息
     * @param roleId
     * @param menus
     * @return
     */
    @PostMapping("{roleId}/permission")
    public R<Boolean> savePermission(@PathVariable Long roleId,@RequestBody Set<Long> menus) {
        return R.success(roleService.savePermission(roleId,menus));
    }

    /**
     * 查询全部角色
     * @return
     */
    public R<List<Role>> selectAll(){
        List<Role> all = roleService.findAll();
        return R.success(all);
    }





}
