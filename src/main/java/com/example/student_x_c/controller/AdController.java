package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.Ad;
import com.example.student_x_c.entity.Shopping;
import com.example.student_x_c.entity.Societies;
import com.example.student_x_c.service.AdService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 广告表
 */
@Slf4j
@RestController
@RequestMapping("/ad")
public class AdController {

    @Autowired
    private AdService adService;

    @Autowired
    private AliOSSUtils aliOSSUtils;


    /**
     * 添加广告
     * @param ad
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "Ad",allEntries = true)
    public R<String> add(@RequestBody Ad ad){
        adService.save(ad);
        return R.success("添加成功");
    }

    /**
     * 管理端进行分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "Ad", key = "#name+'_'+#addressType")
    public R<Page> page(int page, int pageSize, String name,String addressType,String sexType,String studentType) {
        Page<Ad> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Ad> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), Ad::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(addressType), Ad::getAddressType, addressType);
        queryWrapper.like(StringUtils.isNotEmpty(sexType), Ad::getSexType, sexType);
        queryWrapper.like(StringUtils.isNotEmpty(studentType), Ad::getStudentType, studentType);
        queryWrapper.orderByDesc(Ad::getUpdateTime);

        adService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 管理端进行分页查询通过地址
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page/addressType")
    @CacheEvict(value = "Ad",allEntries = true)
    public R<Page> pageByAddressType(int page, int pageSize, String name,String addressType,String sexType,String studentType) {
        Page<Ad> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Ad> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), Ad::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(addressType), Ad::getAddressType, addressType);
        queryWrapper.like(StringUtils.isNotEmpty(sexType), Ad::getSexType, sexType);
        queryWrapper.like(StringUtils.isNotEmpty(studentType), Ad::getStudentType, studentType);
        queryWrapper.orderByDesc(Ad::getUpdateTime);

        adService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 管理端进行分页查询通过地址
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page/studentType")
    @CacheEvict(value = "Ad",allEntries = true)
    public R<Page> pageByStudentType(int page, int pageSize, String name,String addressType,String sexType,String studentType) {
        Page<Ad> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Ad> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), Ad::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(addressType), Ad::getAddressType, addressType);
        queryWrapper.like(StringUtils.isNotEmpty(sexType), Ad::getSexType, sexType);
        queryWrapper.like(StringUtils.isNotEmpty(studentType), Ad::getStudentType, studentType);
        queryWrapper.orderByDesc(Ad::getUpdateTime);

        adService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }


    /**
     * 微信端进行分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize, String name,String addressType,String sexType,String studentType) {
        Page<Ad> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Ad> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), Ad::getName, name);
        queryWrapper.like(StringUtils.isNotEmpty(addressType), Ad::getAddressType, addressType);
        queryWrapper.like(StringUtils.isNotEmpty(sexType), Ad::getSexType, sexType);
        queryWrapper.like(StringUtils.isNotEmpty(studentType), Ad::getStudentType, studentType);
        queryWrapper.orderByDesc(Ad::getUpdateTime);

        List<Ad> lists = new ArrayList<>();
        lists = adService.list(queryWrapper);
        for(Ad list:lists){
            list.setAdImage(aliOSSUtils.getFileUrl(list.getAdImage()));
        }
        log.info("lists:{}",lists);


        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }



    /**
     * 通过id获得广告（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Ad> updateById(@PathVariable Long id){
        Ad ad = adService.getById(id);
        aliOSSUtils.refreshFile(ad.getAdImage());
        return R.success(ad);
    }

    /**
     * 微信端通过id获得图片（便于修改）
     * @param id
     * @return
     */
    @GetMapping("/wx/{id}")
    public R<Ad> updateWxById(@PathVariable Long id){
        Ad ad = adService.getById(id);
        ad.setAdImage(aliOSSUtils.getFileUrl(ad.getAdImage()));
        return R.success(ad);
    }

    /**
     * 更新
     * @param ad
     * @return
     */
    @PostMapping("/update")
    @CacheEvict(value = "Ad",allEntries = true)
    public R<String> update(@RequestBody Ad ad){
        log.info("更新主任务:{}",ad);
        adService.updateById(ad);
        return R.success("更新成功");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "Ad",allEntries = true)
    public R<String> delete(@RequestParam("ids") List<Long> ids) {
        log.info("批量删除:{}",ids);
        adService.removeByIds(ids);
        return R.success("删除成功");
    }

}
