package com.example.student_x_c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.*;
import com.example.student_x_c.service.MainSideService;
import com.example.student_x_c.service.MainTaskService;
import com.example.student_x_c.service.SideQuestsService;
import com.example.student_x_c.service.UserService;
import com.example.student_x_c.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/mainside")
@Slf4j
public class MainSideController {

    @Autowired
    private MainSideService mainSideService;

    @Autowired
    private MainTaskService mainTaskService;

    @Autowired
    private AliOSSUtils aliOSSUtils;

    @Autowired
    private SideQuestsService sideQuestsService;

    @Autowired
    private UserService userService;



    /**
     * 提交任务
     * @param mainSide
     * @return
     */
    @PostMapping("/submit")
    public R<String> submitById(@RequestBody MainSide mainSide) {
        log.info("id:{}" + mainSide);

        //保存mainSide
        mainSideService.save(mainSide);

        LambdaUpdateWrapper<MainSide> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(MainSide::getId, mainSide.getId());
        lambdaUpdateWrapper.set(MainSide::getSub, 1);
        //修改任务状态
        mainSideService.update(lambdaUpdateWrapper);



        return R.success("提交成功");
    }


    /**
     * 分页查询任务提交状况
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/page")
    @Cacheable(value = "mainSide", key = "#name+'_'+'mainSide'")
    public R<Page> page(int page, int pageSize,Long mainId,Integer status,String userName) {
        Page<MainSide> pageInfo = new Page(page, pageSize);
        //通过任务名条件查询
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainSide::getMainId, mainId);

        queryWrapper.like(StringUtils.isNotEmpty(userName),MainSide::getUserName, userName);

        queryWrapper.eq(status != null,MainSide::getSub, status);

        queryWrapper.orderByDesc(MainSide::getUpdateTime);

        mainSideService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }

    /**
     * 微信分页查询自己任务提交状况
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/pagewx")
    public R<Page> pagewx(int page, int pageSize,Long id) {
        Page<MainSide> pageInfo = new Page(page, pageSize);
        //通过任务名条件查询
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainSide::getUserId,id);
        queryWrapper.orderByDesc(MainSide::getUpdateTime);

        List<MainSide> lists = new ArrayList<>();
        lists = mainSideService.list(queryWrapper);
        for(MainSide list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);


        pageInfo.setRecords(lists);

        return R.success(pageInfo);
    }

    /**
     * 分页查询学生自己任务提交状况
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/pageUser")
    public R<Page> pageUser(int page, int pageSize,Long id) {
        Page<MainSide> pageInfo = new Page(page, pageSize);
        //通过任务名条件查询
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        //queryWrapper.like(StringUtils.isNotEmpty(name),MainSide::getName, name);
        queryWrapper.eq(MainSide::getUserId,id);
        queryWrapper.orderByDesc(MainSide::getUpdateTime);
        mainSideService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }


    /**
     * 分页查询学生任务提交状况
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/pageMain")
    @CacheEvict(value = "mainSide",allEntries = true)
    public R<Page> pageMain(int page, int pageSize,@RequestParam("mainId") Long id) {
        log.info("id:{}" , id);
        Page<MainSide> pageInfo = new Page(page, pageSize);
        //通过任务名条件查询
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        //queryWrapper.like(StringUtils.isNotEmpty(name),MainSide::getName, name);
        queryWrapper.eq(MainSide::getMainId,id);
        queryWrapper.orderByDesc(MainSide::getUpdateTime);
        mainSideService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }


    /**
     * 通过id修改任务完成的状态为不通过
     * @param id
     * @return
     */

    @PostMapping("/updateNoById")
    @CacheEvict(value = "mainSide",allEntries = true)
    public R<String> updateNoById(@RequestParam("id") Long id,@RequestParam("mainId") Long mainId,String content) {


        //获取点击id
        LambdaUpdateWrapper<MainSide> updateWrapper = new LambdaUpdateWrapper();
        updateWrapper.eq(MainSide::getUserId,id);
        updateWrapper.eq(MainSide::getMainId,mainId);
        updateWrapper.set(MainSide::getSub,3);
        updateWrapper.set(MainSide::getContent,content);
        mainSideService.update(updateWrapper);

        return R.success("修改成功");
    }



    /**
     * 通过id修改任务完成的状态为通过
     * @param id
     * @return
     */
    @PostMapping("/updateYesById")
    @Transactional
    @CacheEvict(value = "mainSide",allEntries = true)
    public R<String> updateYesById(@RequestParam("id") Long id,@RequestParam("mainId") Long mainId){
        log.info("id:{}",id);
        log.info("mainId:{}",mainId);
        //获取点击id
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainSide::getUserId,id);
        queryWrapper.eq(MainSide::getMainId,mainId);

        //获取mainSide用户
        MainSide one = mainSideService.getOne(queryWrapper);

        //通过id获得相关主任务信息
        LambdaUpdateWrapper<MainTask> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(MainTask::getId,one.getMainId());
        //获取mainTask数值
        MainTask one1 = mainTaskService.getOne(lambdaUpdateWrapper);
        if(one1 == null){
            //获取点击id
            //通过id获得相关主任务信息
            LambdaUpdateWrapper<SideQuests> lambdaUpdateWrapper1 = new LambdaUpdateWrapper<>();
            lambdaUpdateWrapper1.eq(SideQuests::getId,one.getMainId());
            //获取mainTask数值
            SideQuests one2 = sideQuestsService.getOne(lambdaUpdateWrapper1);

            LambdaUpdateWrapper<MainSide> updateWrapper = new LambdaUpdateWrapper();
            updateWrapper.eq(MainSide::getUserId,id);
            updateWrapper.eq(MainSide::getMainId,mainId);
            updateWrapper.set(MainSide::getSub,2);
            mainSideService.update(updateWrapper);

            //个人积分增加相应数量
            LambdaUpdateWrapper<User> updateUserWrapper = new LambdaUpdateWrapper<>();
            updateUserWrapper.eq(User::getId,one.getUserId());
            User user = userService.getOne(updateUserWrapper);
            updateUserWrapper.set(User::getTask,one2.getFraction()+user.getTask());
            userService.update(updateUserWrapper);

            return R.success("修改成功");
        }
        //修改主任务信息
        //获取点击id
        LambdaUpdateWrapper<MainSide> updateWrapper1 = new LambdaUpdateWrapper();
        updateWrapper1.eq(MainSide::getUserId,id);
        updateWrapper1.eq(MainSide::getMainId,mainId);
        updateWrapper1.set(MainSide::getSub,2);
        mainSideService.update(updateWrapper1);

        //个人积分增加相应数量
        LambdaUpdateWrapper<User> updateUserWrapper1 = new LambdaUpdateWrapper<>();
        updateUserWrapper1.eq(User::getId,one.getUserId());
        User user = userService.getOne(updateUserWrapper1);
        updateUserWrapper1.set(User::getTask,one1.getFraction()+user.getTask());
        userService.update(updateUserWrapper1);

        return R.success("修改成功");
    }

    /**
     * 通过mainId和userId查询任务信息
     * @param mainId
     * @return
     */
    @PostMapping("/select")
    @CacheEvict(value = "mainSide",allEntries = true)
    public R<Integer> selectById(Long mainId,Long userId) {
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainSide::getMainId,mainId);
        queryWrapper.eq(MainSide::getUserId,userId);
        MainSide main = mainSideService.getOne(queryWrapper);
        if(main == null){
            return R.success(0);
        }
        Integer sub = main.getSub();
        return R.success(sub);
    }


    /**
     * 通过审核的列表
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("/pageR")
    public R<Page> pageR(int page, int pageSize,Long id){
        Page<MainSide> pageInfo = new Page(page, pageSize);
        //通过任务名条件查询
        LambdaQueryWrapper<MainSide> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MainSide::getUserId,id);
        queryWrapper.eq(MainSide::getSub,2);
        queryWrapper.orderByDesc(MainSide::getUpdateTime);

        List<MainSide> lists = new ArrayList<>();
        lists = mainSideService.list(queryWrapper);
        for(MainSide list:lists){
            list.setImage(aliOSSUtils.getFileUrl(list.getImage()));
        }
        log.info("lists:{}",lists);
        pageInfo.setRecords(lists);
        return R.success(pageInfo);
    }



    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    @CacheEvict(value = "mainSide",allEntries = true)
    public R<String> deleteAll(@RequestParam("ids") List<Long> ids) {
        mainSideService.removeByIds(ids);
        return R.success("删除成功");
    }











}
