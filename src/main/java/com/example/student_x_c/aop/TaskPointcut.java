package com.example.student_x_c.aop;


import com.example.student_x_c.common.R;
import com.example.student_x_c.entity.MethodLock;
import com.example.student_x_c.service.MethodLockService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * 数据库排斥锁
 */
@Aspect
@Configuration
public class TaskPointcut {

    @Autowired
    private MethodLockService methodLockService;

    @Value("${server.port}")
    private String port;

//    /**
//     * 需要加锁的切入点
//     */
//    @Pointcut("execution(public * com.example.student_x_c.controller.UserShoppingController.submitById(..))")
//    public void methodLock(){
//    }
//
//    /**
//     * 事前处理
//     * @param pj
//     * @return
//     */
//    @Around("methodLock()")
//    public R<Object> around(ProceedingJoinPoint pj){
//        String methodName = "";
//        try {
//            MethodSignature signature = (MethodSignature) pj.getSignature();
//            methodName =signature.getMethod().getName();
//            MethodLock methodLock = new MethodLock();
//            methodLock.setMethodName(methodName);
//
//            //方法描述拼接线程和当前接口
//            methodLock.setMethodDesc(Thread.currentThread().getId()+"-"+port);
//            methodLock.setUpdateTime(LocalDateTime.now());
//            //插入数据成功则代表获取锁成功
//            methodLockService.save(methodLock);
//            return R.success(pj.proceed());
//        } catch (Throwable e){
//            return R.error("getLook fail");
//        }
//    }
//
//    /**
//     * 事后处理
//     * @param joinPoint
//     */
//    @After("methodLock()")
//    public void doAfterAdvice(JoinPoint joinPoint){
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//        String methodName = signature.getMethod().getName();
//        //只能释放当前线程拿到的锁的信息
//        MethodLock methodLock = methodLockService.findByMethodNameAndMethodDesc(methodName,Thread.currentThread().getId()+"-"+port);
//        if(methodLock != null){
//            methodLockService.removeById(methodLock.getId());
//        }
//    }
//
//    /**
//     * 异常处理 释放锁
//     */
//    @AfterThrowing("methodLock()")
//    public void afterThrowing(JoinPoint joinPoint){
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//        String methodName = signature.getMethod().getName();
//        MethodLock methodLock = methodLockService.findByMethodNameAndMethodDesc(methodName,Thread.currentThread().getId()+"-"+port);
//        if(methodLock != null){
//            methodLockService.removeById(methodLock.getId());
//        }
//    }
}
