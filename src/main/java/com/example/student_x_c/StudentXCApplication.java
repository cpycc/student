package com.example.student_x_c;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@ServletComponentScan //扫描filter启动拦截器
@EnableTransactionManagement//开启事务注解
@EnableCaching // 开启缓存注解
@ConfigurationPropertiesScan({"com.example.student_x_c.config","com.example.student_x_c.aop"})//开启config注解
@EnableScheduling
public class StudentXCApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentXCApplication.class, args);
    }

}
