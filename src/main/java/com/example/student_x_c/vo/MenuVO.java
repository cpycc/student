package com.example.student_x_c.vo;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class MenuVO {


    private String name;
    private String path;
    private String component;
    private Boolean hidden;
    private Meta meta;


    @Data
    public static class Meta {
        private String title;
        private String icon;
        private boolean noCache;
        private String redirect;
    }

}
