package com.example.student_x_c.vo;

import com.example.student_x_c.dto.PermissionDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
public class UserVO {

    // 用户ID
    private final long id;
    // 用户名称
    private final String name;
    // 用户令牌（如JWT）
    private final String token;

    private final String roles;

    private final List<PermissionDto> permissions;


    public static class Builder {
        // 初始默认值（如果有需要的话）
        private long id;
        private String name;

        private String roles;

        private List<PermissionDto> permissions;
        private String token;

        /**
         * 构造一个空的 Builder 实例。
         */
        public Builder() {
            // 不需要设置默认值时，此构造器可以为空
        }

        /**
         * 设置用户ID。
         */
        public Builder id(long id) {
            this.id = id;
            return this; // 返回 Builder 以支持链式调用
        }

        /**
         * 设置用户名。
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * 设置角色。
         */
        public Builder permissions(List<PermissionDto> permissions) {
            this.permissions = permissions;
            return this;
        }

        /**
         * 设置角色。
         */
        public Builder roles(String roles) {
            this.roles = roles;
            return this;
        }

        /**
         * 设置用户令牌。
         */
        public Builder token(String token) {
            this.token = token;
            return this;
        }

        /**
         * 使用已设置的属性构建 UserVO 实例。
         */
        public UserVO build() {
            Objects.requireNonNull(id, "ID must not be null");
            Objects.requireNonNull(name, "Name must not be null");
            Objects.requireNonNull(roles, "Roles must not be null");
            Objects.requireNonNull(permissions, "Permissions must not be null");

            return new UserVO(id, name, token, roles, permissions);
        }
    }
}