package com.example.student_x_c.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student_x_c.entity.UserShopping;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserShoppingMapper extends BaseMapper<UserShopping> {
}
