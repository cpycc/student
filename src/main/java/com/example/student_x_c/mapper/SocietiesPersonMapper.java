package com.example.student_x_c.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student_x_c.entity.SocietiesPerson;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SocietiesPersonMapper extends BaseMapper<SocietiesPerson> {
}
