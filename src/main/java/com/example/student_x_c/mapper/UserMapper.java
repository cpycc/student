package com.example.student_x_c.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student_x_c.dto.PermissionDto;
import com.example.student_x_c.entity.Role;
import com.example.student_x_c.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 查询用户角色
     * @param userId 用户id
     * @return 角色信息
     */
    @Select("select r.id,r.role_code,r.role_name,r.description,r.enable,r.create_time,r.update_time " +
            "from role r " +
            "INNER JOIN user_role ur ON r.id=ur.role_id where ur.user_id=#{userId} ")
    List<Role> selectUserRoles(Long userId);

    /**
     * 查询用户权限
     * @param userId 用户id
     * @return 权限信息
     */
    @Select("SELECT m.id, m.`path`, m.`name`,  m.`component`,  m.`icon`,  m.`no_cache`, m.`hidden`,  m.`redirect`, m.p_id  " +
            "FROM " +
            "menu m " +
            "INNER JOIN permission p ON p.menu_id = m.id " +
            "INNER JOIN user_role ur ON ur.role_id = p.role_id " +
            "INNER JOIN user u ON u.id = ur.user_id " +
            "INNER JOIN role r ON r.id = ur.role_id where ur.user_id=#{userId}"+
            " and m.enable=1 " +
            " order by m.sort "
    )
    List<PermissionDto> selectUserPermission(Long userId);
}
