package com.example.student_x_c.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student_x_c.entity.MainTask;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MainTaskMapper extends BaseMapper<MainTask> {
}
