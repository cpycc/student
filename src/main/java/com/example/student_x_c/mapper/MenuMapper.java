package com.example.student_x_c.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student_x_c.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据父级菜单id查出下级菜单
     * @param pid 父级菜单id
     * @return 下级菜单列表
     */
    @Select("select * from menu where p_id=#{pid} ")
    List<Menu> selectChilds(Long pid);
}
