package com.example.student_x_c.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDto {

    //权限id
    private Long id;

    //路径
    private String path;

    //名称
    private String name;

    //组件
    private String component;

    //是否隐藏
    private Boolean hidden;

    //图标
    private String icon;

    //是否缓存
    private Boolean noCache;

    //跳转
    private String redirect;

    //父id
    private Long pId;
}
